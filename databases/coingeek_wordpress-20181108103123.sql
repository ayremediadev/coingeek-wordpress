# WordPress MySQL database migration
#
# Generated: Thursday 8. November 2018 10:31 UTC
# Hostname: 127.0.0.1
# Database: `coingeek_wordpress`
# URL: //wordpress.coingeek.staging.fides.io
# Path: /home/coingeek/www/wordpress/public_html
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_sib_model_forms, wp_sib_model_users, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, acf-field, acf-field-group, actualites, attachment, event, nav_menu_item, news, page, post
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un commentateur WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-11-01 05:47:01', '2018-11-01 04:47:01', 'Bonjour, ceci est un commentaire.\nPour débuter avec la modération, la modification et la suppression de commentaires, veuillez visiter l’écran des Commentaires dans le Tableau de bord.\nLes avatars des personnes qui commentent arrivent depuis <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 17, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-02 09:56:47', '2018-11-02 08:56:47', 'ahihi', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1),
(3, 17, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-02 10:01:20', '2018-11-02 09:01:20', 'dfafadfdsf', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1),
(4, 17, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-02 10:03:12', '2018-11-02 09:03:12', 'adsfadsfadsfasdf', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 2, 1),
(5, 16, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-05 11:37:16', '2018-11-05 10:37:16', 'alo', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1),
(6, 14, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-05 11:47:57', '2018-11-05 10:47:57', 'sdfasfasdfdsf', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1),
(7, 17, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-06 05:55:13', '2018-11-06 04:55:13', 'asdfasdf', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 4, 1),
(8, 17, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-06 05:55:58', '2018-11-06 04:55:58', 'ahihi', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 4, 1),
(9, 14, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-06 06:32:30', '2018-11-06 05:32:30', 'this is commenjt test', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1),
(10, 14, 'fidesio', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-06 06:32:44', '2018-11-06 05:32:44', 'this is test', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 9, 1),
(11, 16, 'Admin', 'quyen.le@fidesio.com', '', '127.0.0.1', '2018-11-07 05:48:06', '2018-11-07 04:48:06', 'this is test', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', '', 0, 1) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=627 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wordpress.coingeek.staging.fides.io', 'yes'),
(2, 'home', 'http://wordpress.coingeek.staging.fides.io', 'yes'),
(3, 'blogname', 'coingeek', 'yes'),
(4, 'blogdescription', 'Un site utilisant WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'quyen.le@fidesio.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:144:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:7:"news/?$";s:24:"index.php?post_type=news";s:37:"news/feed/(feed|rdf|rss|rss2|atom)/?$";s:41:"index.php?post_type=news&feed=$matches[1]";s:32:"news/(feed|rdf|rss|rss2|atom)/?$";s:41:"index.php?post_type=news&feed=$matches[1]";s:24:"news/page/([0-9]{1,})/?$";s:42:"index.php?post_type=news&paged=$matches[1]";s:8:"event/?$";s:25:"index.php?post_type=event";s:38:"event/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=event&feed=$matches[1]";s:33:"event/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=event&feed=$matches[1]";s:25:"event/page/([0-9]{1,})/?$";s:43:"index.php?post_type=event&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:53:"new-category/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?category_new=$matches[1]&feed=$matches[2]";s:48:"new-category/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?category_new=$matches[1]&feed=$matches[2]";s:29:"new-category/([^/]+)/embed/?$";s:45:"index.php?category_new=$matches[1]&embed=true";s:41:"new-category/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?category_new=$matches[1]&paged=$matches[2]";s:23:"new-category/([^/]+)/?$";s:34:"index.php?category_new=$matches[1]";s:53:"category_event/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?category_event=$matches[1]&feed=$matches[2]";s:48:"category_event/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?category_event=$matches[1]&feed=$matches[2]";s:29:"category_event/(.+?)/embed/?$";s:47:"index.php?category_event=$matches[1]&embed=true";s:41:"category_event/(.+?)/page/?([0-9]{1,})/?$";s:54:"index.php?category_event=$matches[1]&paged=$matches[2]";s:23:"category_event/(.+?)/?$";s:36:"index.php?category_event=$matches[1]";s:32:"news/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:42:"news/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:62:"news/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"news/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"news/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"news/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:21:"news/([^/]+)/embed/?$";s:37:"index.php?news=$matches[1]&embed=true";s:25:"news/([^/]+)/trackback/?$";s:31:"index.php?news=$matches[1]&tb=1";s:45:"news/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?news=$matches[1]&feed=$matches[2]";s:40:"news/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?news=$matches[1]&feed=$matches[2]";s:33:"news/([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?news=$matches[1]&paged=$matches[2]";s:40:"news/([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?news=$matches[1]&cpage=$matches[2]";s:29:"news/([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?news=$matches[1]&page=$matches[2]";s:21:"news/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:31:"news/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:51:"news/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:46:"news/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:46:"news/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:27:"news/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:33:"event/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"event/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"event/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"event/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"event/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"event/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:22:"event/([^/]+)/embed/?$";s:38:"index.php?event=$matches[1]&embed=true";s:26:"event/([^/]+)/trackback/?$";s:32:"index.php?event=$matches[1]&tb=1";s:46:"event/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?event=$matches[1]&feed=$matches[2]";s:41:"event/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?event=$matches[1]&feed=$matches[2]";s:34:"event/([^/]+)/page/?([0-9]{1,})/?$";s:45:"index.php?event=$matches[1]&paged=$matches[2]";s:41:"event/([^/]+)/comment-page-([0-9]{1,})/?$";s:45:"index.php?event=$matches[1]&cpage=$matches[2]";s:30:"event/([^/]+)(?:/([0-9]+))?/?$";s:44:"index.php?event=$matches[1]&page=$matches[2]";s:22:"event/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"event/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"event/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"event/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"event/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"event/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=10&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:43:"custom-post-type-ui/custom-post-type-ui.php";i:2;s:33:"duplicate-post/duplicate-post.php";i:3;s:21:"mailin/sendinblue.php";i:4;s:47:"regenerate-thumbnails/regenerate-thumbnails.php";i:5;s:51:"sharethis-share-buttons/sharethis-share-buttons.php";i:6;s:37:"user-role-editor/user-role-editor.php";i:7;s:31:"wp-migrate-db/wp-migrate-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'wp-athena', 'yes'),
(41, 'stylesheet', 'wp-athena', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:3:{i:1;a:0:{}i:2;a:4:{s:5:"title";s:0:"";s:4:"text";s:505:"<div class="list">\r\n<h3>News</h3>\r\n<ul>\r\n 	<li><a href="#">Business</a></li>\r\n 	<li><a href="#">Editorial</a></li>\r\n 	<li><a href="#">Technology</a></li>\r\n 	<li><a href="#">Interviews</a></li>\r\n 	<li><a href="#">Videos</a></li>\r\n</ul>\r\n</div>\r\n<div class="list">\r\n<h3>Events</h3>\r\n<ul>\r\n 	<li><a>All events</a></li>\r\n</ul>\r\n</div>\r\n<div class="list">\r\n<h3>BCH Guide</h3>\r\n<ul>\r\n 	<li><a>Bitcoin ATM locator</a></li>\r\n 	<li><a>Merchant list</a></li>\r\n 	<li><a>Exchanges &amp; wallet</a></li>\r\n</ul>\r\n</div>";s:6:"filter";b:1;s:6:"visual";b:1;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:21:"mailin/sendinblue.php";a:2:{i:0;s:11:"SIB_Manager";i:1;s:9:"uninstall";}s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";s:20:"sfsi_Unistall_plugin";}', 'no'),
(82, 'timezone_string', 'Europe/Paris', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '10', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:69:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:10:"copy_posts";b:1;s:14:"ure_edit_roles";b:1;s:16:"ure_create_roles";b:1;s:16:"ure_delete_roles";b:1;s:23:"ure_create_capabilities";b:1;s:23:"ure_delete_capabilities";b:1;s:18:"ure_manage_options";b:1;s:15:"ure_reset_roles";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:10:"copy_posts";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', '', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:11:"home-footer";a:1:{i:0;s:6:"text-2";}s:13:"array_version";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'cron', 'a:6:{i:1541674023;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1541695623;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1541738837;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1541738838;a:1:{s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1541751513;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(136, 'can_compress_scripts', '1', 'no'),
(137, 'theme_mods_twentyseventeen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1541047676;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(138, 'current_theme', 'Athena Wordpress themes', 'yes'),
(139, 'theme_mods_wp-athena', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:16:"athena_main_menu";i:2;}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(140, 'theme_switched', '', 'yes'),
(143, 'recently_activated', 'a:1:{s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";i:1541146675;}', 'yes'),
(144, 'acf_version', '5.7.7', 'yes'),
(153, 'cptui_new_install', 'false', 'yes'),
(161, 'options_at_logo', '5', 'no'),
(162, '_options_at_logo', 'field_5bce6c3b063ee', 'no'),
(170, 'cptui_post_types', 'a:2:{s:4:"news";a:29:{s:4:"name";s:4:"news";s:5:"label";s:4:"News";s:14:"singular_label";s:3:"New";s:11:"description";s:0:"";s:6:"public";s:4:"true";s:18:"publicly_queryable";s:4:"true";s:7:"show_ui";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:12:"show_in_rest";s:4:"true";s:9:"rest_base";s:0:"";s:21:"rest_controller_class";s:0:"";s:11:"has_archive";s:4:"true";s:18:"has_archive_string";s:0:"";s:19:"exclude_from_search";s:5:"false";s:15:"capability_type";s:4:"post";s:12:"hierarchical";s:5:"false";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:0:"";s:17:"rewrite_withfront";s:4:"true";s:9:"query_var";s:4:"true";s:14:"query_var_slug";s:0:"";s:13:"menu_position";s:0:"";s:12:"show_in_menu";s:4:"true";s:19:"show_in_menu_string";s:0:"";s:9:"menu_icon";s:0:"";s:8:"supports";a:6:{i:0;s:5:"title";i:1;s:6:"editor";i:2;s:9:"thumbnail";i:3;s:7:"excerpt";i:4;s:8:"comments";i:5;s:6:"author";}s:10:"taxonomies";a:0:{}s:6:"labels";a:24:{s:9:"menu_name";s:0:"";s:9:"all_items";s:0:"";s:7:"add_new";s:0:"";s:12:"add_new_item";s:0:"";s:9:"edit_item";s:0:"";s:8:"new_item";s:0:"";s:9:"view_item";s:0:"";s:10:"view_items";s:0:"";s:12:"search_items";s:0:"";s:9:"not_found";s:0:"";s:18:"not_found_in_trash";s:0:"";s:17:"parent_item_colon";s:0:"";s:14:"featured_image";s:0:"";s:18:"set_featured_image";s:0:"";s:21:"remove_featured_image";s:0:"";s:18:"use_featured_image";s:0:"";s:8:"archives";s:0:"";s:16:"insert_into_item";s:0:"";s:21:"uploaded_to_this_item";s:0:"";s:17:"filter_items_list";s:0:"";s:21:"items_list_navigation";s:0:"";s:10:"items_list";s:0:"";s:10:"attributes";s:0:"";s:14:"name_admin_bar";s:0:"";}s:15:"custom_supports";s:0:"";}s:5:"event";a:29:{s:4:"name";s:5:"event";s:5:"label";s:6:"Events";s:14:"singular_label";s:6:"Events";s:11:"description";s:0:"";s:6:"public";s:4:"true";s:18:"publicly_queryable";s:4:"true";s:7:"show_ui";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:12:"show_in_rest";s:4:"true";s:9:"rest_base";s:0:"";s:21:"rest_controller_class";s:0:"";s:11:"has_archive";s:4:"true";s:18:"has_archive_string";s:0:"";s:19:"exclude_from_search";s:5:"false";s:15:"capability_type";s:4:"post";s:12:"hierarchical";s:5:"false";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:0:"";s:17:"rewrite_withfront";s:4:"true";s:9:"query_var";s:4:"true";s:14:"query_var_slug";s:0:"";s:13:"menu_position";s:0:"";s:12:"show_in_menu";s:4:"true";s:19:"show_in_menu_string";s:0:"";s:9:"menu_icon";s:0:"";s:8:"supports";a:3:{i:0;s:5:"title";i:1;s:6:"editor";i:2;s:9:"thumbnail";}s:10:"taxonomies";a:0:{}s:6:"labels";a:24:{s:9:"menu_name";s:0:"";s:9:"all_items";s:0:"";s:7:"add_new";s:0:"";s:12:"add_new_item";s:0:"";s:9:"edit_item";s:0:"";s:8:"new_item";s:0:"";s:9:"view_item";s:0:"";s:10:"view_items";s:0:"";s:12:"search_items";s:0:"";s:9:"not_found";s:0:"";s:18:"not_found_in_trash";s:0:"";s:17:"parent_item_colon";s:0:"";s:14:"featured_image";s:0:"";s:18:"set_featured_image";s:0:"";s:21:"remove_featured_image";s:0:"";s:18:"use_featured_image";s:0:"";s:8:"archives";s:0:"";s:16:"insert_into_item";s:0:"";s:21:"uploaded_to_this_item";s:0:"";s:17:"filter_items_list";s:0:"";s:21:"items_list_navigation";s:0:"";s:10:"items_list";s:0:"";s:10:"attributes";s:0:"";s:14:"name_admin_bar";s:0:"";}s:15:"custom_supports";s:0:"";}}', 'yes'),
(181, 'duplicate_post_copytitle', '1', 'yes'),
(182, 'duplicate_post_copydate', '', 'yes'),
(183, 'duplicate_post_copystatus', '', 'yes'),
(184, 'duplicate_post_copyslug', '', 'yes'),
(185, 'duplicate_post_copyexcerpt', '1', 'yes'),
(186, 'duplicate_post_copycontent', '1', 'yes'),
(187, 'duplicate_post_copythumbnail', '1', 'yes'),
(188, 'duplicate_post_copytemplate', '1', 'yes'),
(189, 'duplicate_post_copyformat', '1', 'yes'),
(190, 'duplicate_post_copyauthor', '', 'yes'),
(191, 'duplicate_post_copypassword', '', 'yes'),
(192, 'duplicate_post_copyattachments', '', 'yes'),
(193, 'duplicate_post_copychildren', '', 'yes'),
(194, 'duplicate_post_copycomments', '', 'yes'),
(195, 'duplicate_post_copymenuorder', '1', 'yes'),
(196, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(197, 'duplicate_post_blacklist', '', 'yes'),
(198, 'duplicate_post_types_enabled', 'a:4:{i:0;s:4:"post";i:1;s:4:"page";i:2;s:4:"news";i:3;s:5:"event";}', 'yes'),
(199, 'duplicate_post_show_row', '1', 'yes'),
(200, 'duplicate_post_show_adminbar', '1', 'yes'),
(201, 'duplicate_post_show_submitbox', '1', 'yes'),
(202, 'duplicate_post_show_bulkactions', '1', 'yes'),
(203, 'duplicate_post_version', '3.2.2', 'yes'),
(204, 'duplicate_post_show_notice', '0', 'no'),
(205, 'duplicate_post_title_prefix', '', 'yes'),
(206, 'duplicate_post_title_suffix', '', 'yes'),
(207, 'duplicate_post_increase_menu_order_by', '', 'yes'),
(208, 'duplicate_post_roles', 'a:2:{i:0;s:13:"administrator";i:1;s:6:"editor";}', 'yes'),
(211, 'new_admin_email', 'quyen.le@fidesio.com', 'yes'),
(225, 'options_footer_logo', '5', 'no'),
(226, '_options_footer_logo', 'field_5bdad01a9af17', 'no'),
(227, 'options_socials_0_social_link', 'https://www.facebook.com/realcoingeek/', 'no'),
(228, '_options_socials_0_social_link', 'field_5bdad1ea5ce2b', 'no'),
(229, 'options_socials_0_social_class', 'fab fa-facebook-f', 'no'),
(230, '_options_socials_0_social_class', 'field_5bdad1fc5ce2c', 'no'),
(231, 'options_socials_1_social_link', 'https://twitter.com/realcoingeek', 'no'),
(232, '_options_socials_1_social_link', 'field_5bdad1ea5ce2b', 'no'),
(233, 'options_socials_1_social_class', 'fab fa-twitter', 'no'),
(234, '_options_socials_1_social_class', 'field_5bdad1fc5ce2c', 'no'),
(235, 'options_socials_2_social_link', 'https://www.youtube.com/channel/UC95_Nqes9m5arhoT1lt1SFg', 'no'),
(236, '_options_socials_2_social_link', 'field_5bdad1ea5ce2b', 'no'),
(237, 'options_socials_2_social_class', 'fab fa-youtube', 'no'),
(238, '_options_socials_2_social_class', 'field_5bdad1fc5ce2c', 'no'),
(239, 'options_socials', '3', 'no'),
(240, '_options_socials', 'field_5bdad1b13543a', 'no'),
(241, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(247, 'sib_main_option', 'a:2:{s:10:"access_key";s:16:"RdcrwI538COASzD0";s:6:"ma_key";s:0:"";}', 'yes'),
(248, 'sib_home_option', 'a:5:{s:14:"activate_email";s:2:"no";s:11:"activate_ma";s:2:"no";s:6:"sender";i:1;s:9:"from_name";s:4:"test";s:10:"from_email";s:24:"quyenltv54cntt@gmail.com";}', 'yes'),
(249, 'sib_token_store', 'a:1:{s:12:"access_token";s:32:"bf28a4d1c7bfcc38938b7f2b0f0d238f";}', 'yes'),
(250, 'sib_use_apiv2', '1', 'yes'),
(251, 'widget_sib_subscribe_form', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(268, 'sib_temp_list', '4', 'yes'),
(291, 'category_children', 'a:0:{}', 'yes'),
(295, 'cptui_taxonomies', 'a:2:{s:12:"category_new";a:24:{s:4:"name";s:12:"category_new";s:5:"label";s:10:"Categories";s:14:"singular_label";s:10:"Categories";s:11:"description";s:0:"";s:6:"public";s:4:"true";s:18:"publicly_queryable";s:4:"true";s:12:"hierarchical";s:4:"true";s:7:"show_ui";s:4:"true";s:12:"show_in_menu";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:9:"query_var";s:4:"true";s:14:"query_var_slug";s:0:"";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:12:"new-category";s:17:"rewrite_withfront";s:1:"1";s:20:"rewrite_hierarchical";s:1:"0";s:17:"show_admin_column";s:5:"false";s:12:"show_in_rest";s:5:"false";s:18:"show_in_quick_edit";s:0:"";s:9:"rest_base";s:0:"";s:21:"rest_controller_class";s:0:"";s:6:"labels";a:18:{s:9:"menu_name";s:0:"";s:9:"all_items";s:0:"";s:9:"edit_item";s:0:"";s:9:"view_item";s:0:"";s:11:"update_item";s:0:"";s:12:"add_new_item";s:0:"";s:13:"new_item_name";s:0:"";s:11:"parent_item";s:0:"";s:17:"parent_item_colon";s:0:"";s:12:"search_items";s:0:"";s:13:"popular_items";s:0:"";s:26:"separate_items_with_commas";s:0:"";s:19:"add_or_remove_items";s:0:"";s:21:"choose_from_most_used";s:0:"";s:9:"not_found";s:0:"";s:8:"no_terms";s:0:"";s:21:"items_list_navigation";s:0:"";s:10:"items_list";s:0:"";}s:11:"meta_box_cb";s:0:"";s:12:"object_types";a:1:{i:0;s:4:"news";}}s:14:"category_event";a:24:{s:4:"name";s:14:"category_event";s:5:"label";s:10:"Categories";s:14:"singular_label";s:10:"Categories";s:11:"description";s:0:"";s:6:"public";s:4:"true";s:18:"publicly_queryable";s:4:"true";s:12:"hierarchical";s:4:"true";s:7:"show_ui";s:4:"true";s:12:"show_in_menu";s:4:"true";s:17:"show_in_nav_menus";s:4:"true";s:9:"query_var";s:4:"true";s:14:"query_var_slug";s:0:"";s:7:"rewrite";s:4:"true";s:12:"rewrite_slug";s:0:"";s:17:"rewrite_withfront";s:1:"1";s:20:"rewrite_hierarchical";s:1:"1";s:17:"show_admin_column";s:5:"false";s:12:"show_in_rest";s:5:"false";s:18:"show_in_quick_edit";s:0:"";s:9:"rest_base";s:0:"";s:21:"rest_controller_class";s:0:"";s:6:"labels";a:18:{s:9:"menu_name";s:0:"";s:9:"all_items";s:0:"";s:9:"edit_item";s:0:"";s:9:"view_item";s:0:"";s:11:"update_item";s:0:"";s:12:"add_new_item";s:0:"";s:13:"new_item_name";s:0:"";s:11:"parent_item";s:0:"";s:17:"parent_item_colon";s:0:"";s:12:"search_items";s:0:"";s:13:"popular_items";s:0:"";s:26:"separate_items_with_commas";s:0:"";s:19:"add_or_remove_items";s:0:"";s:21:"choose_from_most_used";s:0:"";s:9:"not_found";s:0:"";s:8:"no_terms";s:0:"";s:21:"items_list_navigation";s:0:"";s:10:"items_list";s:0:"";}s:11:"meta_box_cb";s:0:"";s:12:"object_types";a:1:{i:0;s:5:"event";}}}', 'yes'),
(342, 'adding_tags', 'yes', 'yes'),
(344, 'show_new_notification', 'yes', 'yes'),
(345, 'show_premium_cumulative_count_notification', 'yes', 'yes'),
(346, 'sfsi_section1_options', 's:410:"a:11:{s:16:"sfsi_rss_display";s:3:"yes";s:18:"sfsi_email_display";s:3:"yes";s:21:"sfsi_facebook_display";s:3:"yes";s:20:"sfsi_twitter_display";s:3:"yes";s:19:"sfsi_google_display";s:3:"yes";s:22:"sfsi_pinterest_display";s:2:"no";s:22:"sfsi_instagram_display";s:2:"no";s:21:"sfsi_linkedin_display";s:2:"no";s:20:"sfsi_youtube_display";s:2:"no";s:19:"sfsi_custom_display";s:0:"";s:17:"sfsi_custom_files";s:0:"";}";', 'yes'),
(347, 'sfsi_section2_options', 's:1770:"a:36:{s:12:"sfsi_rss_url";s:24:"http://coingeek.me/feed/";s:14:"sfsi_rss_icons";s:5:"email";s:14:"sfsi_email_url";s:297:"http://www.specificfeeds.com/widgets/emailSubscribeEncFeed/K3FwYTJjd1FLaWZHNks5SU1NVEpCQlpNN296M0tnTEtkOWJ2YlVNbmZzLzNKUG9tNDdmSERFL0hsdjFMVzhUaHpXMy93MEJMOVNyUW52bDk2bGQ1SklFc2pacDVscTVhTER1TWM3MjJJWEpHOTRYMEUzUTNtRUYxeXcxSVlRb0x8alp2SkNPdFJlR2syaURrS3VramdsbmpkcG1vUjBQYVRVN1NZNExOQnZpbz0=/OA==/";s:24:"sfsi_facebookPage_option";s:2:"no";s:21:"sfsi_facebookPage_url";s:0:"";s:24:"sfsi_facebookLike_option";s:3:"yes";s:25:"sfsi_facebookShare_option";s:3:"yes";s:21:"sfsi_twitter_followme";s:2:"no";s:27:"sfsi_twitter_followUserName";s:0:"";s:22:"sfsi_twitter_aboutPage";s:3:"yes";s:17:"sfsi_twitter_page";s:2:"no";s:20:"sfsi_twitter_pageURL";s:0:"";s:26:"sfsi_twitter_aboutPageText";s:82:"Hey, check out this cool site I found: www.yourname.com #Topic via@my_twitter_name";s:16:"sfsi_google_page";s:2:"no";s:19:"sfsi_google_pageURL";s:0:"";s:22:"sfsi_googleLike_option";s:3:"yes";s:23:"sfsi_googleShare_option";s:3:"yes";s:20:"sfsi_youtube_pageUrl";s:0:"";s:17:"sfsi_youtube_page";s:2:"no";s:24:"sfsi_youtubeusernameorid";s:0:"";s:17:"sfsi_ytube_chnlid";s:0:"";s:19:"sfsi_youtube_follow";s:2:"no";s:19:"sfsi_pinterest_page";s:2:"no";s:22:"sfsi_pinterest_pageUrl";s:0:"";s:23:"sfsi_pinterest_pingBlog";s:0:"";s:19:"sfsi_instagram_page";s:2:"no";s:22:"sfsi_instagram_pageUrl";s:0:"";s:18:"sfsi_linkedin_page";s:2:"no";s:21:"sfsi_linkedin_pageURL";s:0:"";s:20:"sfsi_linkedin_follow";s:2:"no";s:27:"sfsi_linkedin_followCompany";s:0:"";s:23:"sfsi_linkedin_SharePage";s:3:"yes";s:30:"sfsi_linkedin_recommendBusines";s:2:"no";s:30:"sfsi_linkedin_recommendCompany";s:0:"";s:32:"sfsi_linkedin_recommendProductId";s:0:"";s:21:"sfsi_CustomIcon_links";s:0:"";}";', 'yes'),
(348, 'sfsi_section3_options', 's:272:"a:7:{s:14:"sfsi_mouseOver";s:2:"no";s:21:"sfsi_mouseOver_effect";s:7:"fade_in";s:18:"sfsi_shuffle_icons";s:2:"no";s:22:"sfsi_shuffle_Firstload";s:2:"no";s:21:"sfsi_shuffle_interval";s:2:"no";s:25:"sfsi_shuffle_intervalTime";s:0:"";s:18:"sfsi_actvite_theme";s:7:"default";}";', 'yes'),
(349, 'sfsi_section4_options', 's:1700:"a:43:{s:19:"sfsi_display_counts";s:2:"no";s:24:"sfsi_email_countsDisplay";s:2:"no";s:21:"sfsi_email_countsFrom";s:6:"source";s:23:"sfsi_email_manualCounts";s:2:"20";s:22:"sfsi_rss_countsDisplay";s:2:"no";s:21:"sfsi_rss_manualCounts";s:2:"20";s:22:"sfsi_facebook_PageLink";s:0:"";s:27:"sfsi_facebook_countsDisplay";s:2:"no";s:24:"sfsi_facebook_countsFrom";s:6:"manual";s:26:"sfsi_facebook_manualCounts";s:2:"20";s:26:"sfsi_twitter_countsDisplay";s:2:"no";s:23:"sfsi_twitter_countsFrom";s:6:"manual";s:25:"sfsi_twitter_manualCounts";s:2:"20";s:19:"sfsi_google_api_key";s:0:"";s:25:"sfsi_google_countsDisplay";s:2:"no";s:22:"sfsi_google_countsFrom";s:6:"manual";s:24:"sfsi_google_manualCounts";s:2:"20";s:27:"sfsi_linkedIn_countsDisplay";s:2:"no";s:24:"sfsi_linkedIn_countsFrom";s:6:"manual";s:26:"sfsi_linkedIn_manualCounts";s:2:"20";s:10:"ln_api_key";s:0:"";s:13:"ln_secret_key";s:0:"";s:19:"ln_oAuth_user_token";s:0:"";s:10:"ln_company";s:0:"";s:24:"sfsi_youtubeusernameorid";s:4:"name";s:17:"sfsi_youtube_user";s:0:"";s:22:"sfsi_youtube_channelId";s:0:"";s:17:"sfsi_ytube_chnlid";s:0:"";s:26:"sfsi_youtube_countsDisplay";s:2:"no";s:23:"sfsi_youtube_countsFrom";s:6:"manual";s:25:"sfsi_youtube_manualCounts";s:2:"20";s:28:"sfsi_pinterest_countsDisplay";s:2:"no";s:25:"sfsi_pinterest_countsFrom";s:6:"manual";s:27:"sfsi_pinterest_manualCounts";s:2:"20";s:19:"sfsi_pinterest_user";s:0:"";s:20:"sfsi_pinterest_board";s:0:"";s:25:"sfsi_instagram_countsFrom";s:6:"manual";s:28:"sfsi_instagram_countsDisplay";s:2:"no";s:27:"sfsi_instagram_manualCounts";s:2:"20";s:19:"sfsi_instagram_User";s:0:"";s:23:"sfsi_instagram_clientid";s:0:"";s:21:"sfsi_instagram_appurl";s:0:"";s:20:"sfsi_instagram_token";s:0:"";}";', 'yes'),
(350, 'sfsi_section5_options', 's:1214:"a:29:{s:15:"sfsi_icons_size";s:2:"40";s:18:"sfsi_icons_spacing";s:1:"5";s:20:"sfsi_icons_Alignment";s:4:"left";s:17:"sfsi_icons_perRow";s:1:"5";s:24:"sfsi_icons_ClickPageOpen";s:3:"yes";s:26:"sfsi_icons_suppress_errors";s:2:"no";s:16:"sfsi_icons_stick";s:2:"no";s:18:"sfsi_rssIcon_order";s:1:"1";s:20:"sfsi_emailIcon_order";s:1:"2";s:23:"sfsi_facebookIcon_order";s:1:"3";s:21:"sfsi_googleIcon_order";s:1:"4";s:22:"sfsi_twitterIcon_order";s:1:"5";s:22:"sfsi_youtubeIcon_order";s:1:"7";s:24:"sfsi_pinterestIcon_order";s:1:"8";s:23:"sfsi_linkedinIcon_order";s:1:"9";s:24:"sfsi_instagramIcon_order";s:2:"10";s:22:"sfsi_CustomIcons_order";s:0:"";s:22:"sfsi_rss_MouseOverText";s:3:"RSS";s:24:"sfsi_email_MouseOverText";s:15:"Follow by Email";s:26:"sfsi_twitter_MouseOverText";s:7:"Twitter";s:27:"sfsi_facebook_MouseOverText";s:8:"Facebook";s:25:"sfsi_google_MouseOverText";s:7:"Google+";s:27:"sfsi_linkedIn_MouseOverText";s:8:"LinkedIn";s:28:"sfsi_pinterest_MouseOverText";s:9:"Pinterest";s:28:"sfsi_instagram_MouseOverText";s:9:"Instagram";s:26:"sfsi_youtube_MouseOverText";s:7:"YouTube";s:26:"sfsi_custom_MouseOverTexts";s:0:"";s:23:"sfsi_custom_social_hide";s:2:"no";s:32:"sfsi_pplus_icons_suppress_errors";s:2:"no";}";', 'yes'),
(351, 'sfsi_section6_options', 's:523:"a:14:{s:17:"sfsi_show_Onposts";s:2:"no";s:18:"sfsi_show_Onbottom";s:2:"no";s:22:"sfsi_icons_postPositon";s:6:"source";s:20:"sfsi_icons_alignment";s:12:"center-right";s:22:"sfsi_rss_countsDisplay";s:2:"no";s:20:"sfsi_textBefor_icons";s:26:"Please follow and like us:";s:24:"sfsi_icons_DisplayCounts";s:2:"no";s:12:"sfsi_rectsub";s:3:"yes";s:11:"sfsi_rectfb";s:3:"yes";s:11:"sfsi_rectgp";s:3:"yes";s:12:"sfsi_rectshr";s:2:"no";s:13:"sfsi_recttwtr";s:3:"yes";s:14:"sfsi_rectpinit";s:3:"yes";s:16:"sfsi_rectfbshare";s:3:"yes";}";', 'yes'),
(352, 'sfsi_section7_options', 's:666:"a:15:{s:15:"sfsi_show_popup";s:2:"no";s:15:"sfsi_popup_text";s:42:"Enjoy this blog? Please spread the word :)";s:27:"sfsi_popup_background_color";s:7:"#eff7f7";s:23:"sfsi_popup_border_color";s:7:"#f3faf2";s:27:"sfsi_popup_border_thickness";s:1:"1";s:24:"sfsi_popup_border_shadow";s:3:"yes";s:15:"sfsi_popup_font";s:26:"Helvetica,Arial,sans-serif";s:19:"sfsi_popup_fontSize";s:2:"30";s:20:"sfsi_popup_fontStyle";s:6:"normal";s:20:"sfsi_popup_fontColor";s:7:"#000000";s:17:"sfsi_Show_popupOn";s:4:"none";s:25:"sfsi_Show_popupOn_PageIDs";s:0:"";s:14:"sfsi_Shown_pop";s:8:"ETscroll";s:24:"sfsi_Shown_popupOnceTime";s:0:"";s:32:"sfsi_Shown_popuplimitPerUserTime";s:0:"";}";', 'yes'),
(353, 'sfsi_section8_options', 's:1208:"a:26:{s:20:"sfsi_form_adjustment";s:3:"yes";s:16:"sfsi_form_height";s:3:"180";s:15:"sfsi_form_width";s:3:"230";s:16:"sfsi_form_border";s:2:"no";s:26:"sfsi_form_border_thickness";s:1:"1";s:22:"sfsi_form_border_color";s:7:"#b5b5b5";s:20:"sfsi_form_background";s:7:"#ffffff";s:22:"sfsi_form_heading_text";s:22:"Get new posts by email";s:22:"sfsi_form_heading_font";s:26:"Helvetica,Arial,sans-serif";s:27:"sfsi_form_heading_fontstyle";s:4:"bold";s:27:"sfsi_form_heading_fontcolor";s:7:"#000000";s:26:"sfsi_form_heading_fontsize";s:2:"16";s:27:"sfsi_form_heading_fontalign";s:6:"center";s:20:"sfsi_form_field_text";s:9:"Subscribe";s:20:"sfsi_form_field_font";s:26:"Helvetica,Arial,sans-serif";s:25:"sfsi_form_field_fontstyle";s:6:"normal";s:25:"sfsi_form_field_fontcolor";s:7:"#000000";s:24:"sfsi_form_field_fontsize";s:2:"14";s:25:"sfsi_form_field_fontalign";s:6:"center";s:21:"sfsi_form_button_text";s:9:"Subscribe";s:21:"sfsi_form_button_font";s:26:"Helvetica,Arial,sans-serif";s:26:"sfsi_form_button_fontstyle";s:4:"bold";s:26:"sfsi_form_button_fontcolor";s:7:"#000000";s:25:"sfsi_form_button_fontsize";s:2:"16";s:26:"sfsi_form_button_fontalign";s:6:"center";s:27:"sfsi_form_button_background";s:7:"#dedede";}";', 'yes'),
(354, 'sfsi_section9_options', 's:420:"a:10:{s:20:"sfsi_show_via_widget";s:2:"no";s:16:"sfsi_icons_float";s:2:"no";s:24:"sfsi_icons_floatPosition";s:12:"center-right";s:26:"sfsi_icons_floatMargin_top";s:0:"";s:29:"sfsi_icons_floatMargin_bottom";s:0:"";s:27:"sfsi_icons_floatMargin_left";s:0:"";s:28:"sfsi_icons_floatMargin_right";s:0:"";s:23:"sfsi_disable_floaticons";s:2:"no";s:23:"sfsi_show_via_shortcode";s:2:"no";s:24:"sfsi_show_via_afterposts";s:2:"no";}";', 'yes'),
(355, 'sfsi_feed_id', 'K3FwYTJjd1FLaWZHNks5SU1NVEpCQlpNN296M0tnTEtkOWJ2YlVNbmZzLzNKUG9tNDdmSERFL0hsdjFMVzhUaHpXMy93MEJMOVNyUW52bDk2bGQ1SklFc2pacDVscTVhTER1TWM3MjJJWEpHOTRYMEUzUTNtRUYxeXcxSVlRb0x8alp2SkNPdFJlR2syaURrS3VramdsbmpkcG1vUjBQYVRVN1NZNExOQnZpbz0=', 'yes'),
(356, 'sfsi_redirect_url', 'http://www.specificfeeds.com/widgets/emailSubscribeEncFeed/K3FwYTJjd1FLaWZHNks5SU1NVEpCQlpNN296M0tnTEtkOWJ2YlVNbmZzLzNKUG9tNDdmSERFL0hsdjFMVzhUaHpXMy93MEJMOVNyUW52bDk2bGQ1SklFc2pacDVscTVhTER1TWM3MjJJWEpHOTRYMEUzUTNtRUYxeXcxSVlRb0x8alp2SkNPdFJlR2syaURrS3VramdsbmpkcG1vUjBQYVRVN1NZNExOQnZpbz0=/OA==/', 'yes'),
(357, 'sfsi_installDate', '2018-11-02 08:16:30', 'yes'),
(358, 'sfsi_RatingDiv', 'no', 'yes'),
(359, 'sfsi_footer_sec', 'no', 'yes'),
(360, 'sfsi_activate', '0', 'yes'),
(361, 'sfsi_instagram_sf_count', 's:93:"a:3:{s:4:"date";i:1541116800;s:13:"sfsi_sf_count";s:0:"";s:20:"sfsi_instagram_count";s:0:"";}";', 'yes'),
(362, 'sfsi_error_reporting_notice_dismissed', '1', 'yes'),
(363, 'widget_sfsi-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(364, 'widget_subscriber_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(365, 'sfsi_pluginVersion', '2.06', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(366, 'sfsi_serverphpVersionnotification', 'yes', 'yes'),
(367, 'show_premium_notification', 'yes', 'yes'),
(368, 'show_notification', 'yes', 'yes'),
(369, 'show_mobile_notification', 'yes', 'yes'),
(370, 'sfsi_languageNotice', 'yes', 'yes'),
(371, 'sfsi_pplus_error_reporting_notice_dismissed', '1', 'yes'),
(372, 'sfsi_addThis_icon_removal_notice_dismissed', '1', 'yes'),
(373, 'sfsi_verificatiom_code', 'WgE9WUautDuESvpReZDj', 'yes'),
(380, 'sharethis_inline_settings', 'a:15:{s:25:"sharethis_inline_post_top";s:4:"true";s:36:"sharethis_inline_post_top_margin_top";s:1:"0";s:39:"sharethis_inline_post_top_margin_bottom";s:1:"0";s:28:"sharethis_inline_post_bottom";s:5:"false";s:39:"sharethis_inline_post_bottom_margin_top";s:1:"0";s:42:"sharethis_inline_post_bottom_margin_bottom";s:1:"0";s:25:"sharethis_inline_page_top";s:5:"false";s:36:"sharethis_inline_page_top_margin_top";s:1:"0";s:39:"sharethis_inline_page_top_margin_bottom";s:1:"0";s:28:"sharethis_inline_page_bottom";s:5:"false";s:39:"sharethis_inline_page_bottom_margin_top";s:1:"0";s:42:"sharethis_inline_page_bottom_margin_bottom";s:1:"0";s:17:"sharethis_excerpt";s:5:"false";s:28:"sharethis_excerpt_margin_top";s:1:"0";s:31:"sharethis_excerpt_margin_bottom";s:1:"0";}', 'yes'),
(381, 'sharethis_sticky_settings', 'a:7:{s:21:"sharethis_sticky_home";s:5:"false";s:21:"sharethis_sticky_post";s:5:"false";s:29:"sharethis_sticky_custom_posts";s:4:"true";s:21:"sharethis_sticky_page";s:5:"false";s:25:"sharethis_sticky_category";s:5:"false";s:21:"sharethis_sticky_tags";s:4:"true";s:23:"sharethis_sticky_author";s:4:"true";}', 'yes'),
(382, 'sharethis_sticky_page_off', '', 'yes'),
(383, 'sharethis_sticky_category_off', '', 'yes'),
(384, 'widget_st_button_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(386, 'sharethis_button_config', 'a:2:{s:6:"sticky";a:15:{s:9:"alignment";s:4:"left";s:7:"enabled";s:4:"true";s:6:"labels";s:3:"cta";s:9:"min_count";s:2:"10";s:6:"radius";s:1:"5";s:8:"networks";a:4:{i:0;s:8:"facebook";i:1;s:7:"twitter";i:2;s:8:"linkedin";i:3;s:9:"sharethis";}s:17:"mobile_breakpoint";s:4:"1024";s:3:"top";s:3:"160";s:11:"show_mobile";s:5:"false";s:10:"show_total";s:5:"false";s:12:"show_desktop";s:5:"false";s:17:"use_native_counts";s:4:"true";s:8:"language";s:2:"en";s:10:"updated_at";s:13:"1541147081358";s:2:"ts";s:13:"1541147081358";}s:6:"inline";a:15:{s:9:"alignment";s:6:"center";s:7:"enabled";s:5:"false";s:9:"font_size";s:2:"11";s:6:"labels";s:3:"cta";s:9:"min_count";s:2:"10";s:7:"padding";s:1:"8";s:6:"radius";s:1:"4";s:10:"show_total";s:4:"true";s:17:"use_native_counts";s:4:"true";s:4:"size";s:2:"32";s:7:"spacing";s:1:"8";s:8:"language";s:2:"en";s:10:"updated_at";s:13:"1541147081664";s:2:"ts";s:13:"1541147081664";s:8:"networks";N;}}', 'yes'),
(387, 'sharethis_first_product', 'sticky', 'yes'),
(388, 'sharethis_sticky', 'true', 'yes'),
(389, 'sharethis_property_id', '5bdc08e9c7a9470012145c16-39968e10-de78-11e8-b4e6-1398e418d32c', 'yes'),
(390, 'sharethis_token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1YmRjMDhlOWI3ODlkYjAwMTFjZGQ5YjciLCJlbWFpbCI6InF1eWVuLmxlQGZpZGVzaW8uY29tIn0.DuwXS9ANITkD2E_Fy9bmol2URNyP4lH2ku7iGyYi18E', 'yes'),
(391, 'sharethis_inline', 'false', 'yes'),
(392, 'sharethis_shortcode', '', 'yes'),
(393, 'sharethis_template', '', 'yes'),
(440, 'user_role_editor', 'a:1:{s:11:"ure_version";s:4:"4.46";}', 'yes'),
(441, 'wp_backup_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:10:"copy_posts";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:10:"copy_posts";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'no'),
(442, 'ure_tasks_queue', 'a:0:{}', 'yes'),
(449, 'category_event_children', 'a:0:{}', 'yes'),
(519, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1541673083;}', 'no'),
(550, 'options_footer_image_0_image', '110', 'no'),
(551, '_options_footer_image_0_image', 'field_5be2a410f2ba2', 'no'),
(552, 'options_footer_image_0_link', 'https://calvinayre.com/', 'no'),
(553, '_options_footer_image_0_link', 'field_5be2a41cf2ba3', 'no'),
(554, 'options_footer_image_1_image', '109', 'no'),
(555, '_options_footer_image_1_image', 'field_5be2a410f2ba2', 'no'),
(556, 'options_footer_image_1_link', 'http://ayre.ag', 'no'),
(557, '_options_footer_image_1_link', 'field_5be2a41cf2ba3', 'no'),
(558, 'options_footer_image_2_image', '108', 'no'),
(559, '_options_footer_image_2_image', 'field_5be2a410f2ba2', 'no'),
(560, 'options_footer_image_2_link', 'http://ayre.ag/ventures/calvin-ayre-media-group/', 'no'),
(561, '_options_footer_image_2_link', 'field_5be2a41cf2ba3', 'no'),
(562, 'options_footer_image_3_image', '111', 'no'),
(563, '_options_footer_image_3_image', 'field_5be2a410f2ba2', 'no'),
(564, 'options_footer_image_3_link', 'http://www.calvinayrefoundation.org/', 'no'),
(565, '_options_footer_image_3_link', 'field_5be2a41cf2ba3', 'no'),
(566, 'options_footer_image', '4', 'no'),
(567, '_options_footer_image', 'field_5be2a3e9f2ba1', 'no'),
(610, 'category_new_children', 'a:0:{}', 'yes'),
(613, 'options_logo_home_footer', '142', 'no'),
(614, '_options_logo_home_footer', 'field_5be3a2e8dffbe', 'no'),
(615, 'options_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam suscipit dui in quam luctus scelerisque. Pellentesque tristique felis ut arcu pellentesque sollicitudin.', 'no'),
(616, '_options_description', 'field_5be3a2f5dffbf', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=708 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2018/11/logo.svg'),
(4, 6, '_edit_lock', '1541067436:1'),
(6, 3, '_edit_lock', '1541060167:1'),
(7, 10, '_edit_last', '1'),
(8, 10, '_edit_lock', '1541671267:1'),
(9, 10, '_wp_page_template', 'pages/home.php'),
(10, 12, '_edit_last', '1'),
(11, 12, '_edit_lock', '1541065397:1'),
(12, 13, '_wp_attached_file', '2018/11/item-home.jpg'),
(13, 13, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:180;s:6:"height";i:139;s:4:"file";s:21:"2018/11/item-home.jpg";s:5:"sizes";a:3:{s:15:"img_latest_news";a:4:{s:4:"file";s:21:"item-home-180x139.jpg";s:5:"width";i:180;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:20:"item-home-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:21:"item-home-150x139.jpg";s:5:"width";i:150;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(14, 12, '_thumbnail_id', '13'),
(15, 14, '_thumbnail_id', '88'),
(16, 14, '_dp_original', '12'),
(17, 15, '_thumbnail_id', '91'),
(19, 15, '_dp_original', '14'),
(20, 15, '_edit_lock', '1541661882:1'),
(21, 15, '_edit_last', '1'),
(22, 14, '_edit_lock', '1541661900:1'),
(23, 14, '_edit_last', '1'),
(24, 16, '_thumbnail_id', '87'),
(26, 16, '_dp_original', '14'),
(27, 17, '_thumbnail_id', '86'),
(29, 17, '_dp_original', '14'),
(30, 16, '_edit_lock', '1541661926:1'),
(31, 16, '_edit_last', '1'),
(32, 17, '_edit_lock', '1541661950:1'),
(33, 17, '_edit_last', '1'),
(34, 6, '_edit_last', '1'),
(35, 20, '_wp_attached_file', '2018/11/Coingeek.svg'),
(36, 24, '_menu_item_type', 'post_type'),
(37, 24, '_menu_item_menu_item_parent', '0'),
(38, 24, '_menu_item_object_id', '10'),
(39, 24, '_menu_item_object', 'page'),
(40, 24, '_menu_item_target', ''),
(41, 24, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(42, 24, '_menu_item_xfn', ''),
(43, 24, '_menu_item_url', ''),
(99, 31, '_menu_item_type', 'post_type'),
(100, 31, '_menu_item_menu_item_parent', '0'),
(101, 31, '_menu_item_object_id', '10'),
(102, 31, '_menu_item_object', 'page'),
(103, 31, '_menu_item_target', ''),
(104, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(105, 31, '_menu_item_xfn', ''),
(106, 31, '_menu_item_url', ''),
(108, 32, '_menu_item_type', 'post_type'),
(109, 32, '_menu_item_menu_item_parent', '0'),
(110, 32, '_menu_item_object_id', '2'),
(111, 32, '_menu_item_object', 'page'),
(112, 32, '_menu_item_target', ''),
(113, 32, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(114, 32, '_menu_item_xfn', ''),
(115, 32, '_menu_item_url', ''),
(117, 33, '_menu_item_type', 'post_type'),
(118, 33, '_menu_item_menu_item_parent', '0'),
(119, 33, '_menu_item_object_id', '10'),
(120, 33, '_menu_item_object', 'page'),
(121, 33, '_menu_item_target', ''),
(122, 33, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(123, 33, '_menu_item_xfn', ''),
(124, 33, '_menu_item_url', ''),
(126, 34, '_menu_item_type', 'post_type'),
(127, 34, '_menu_item_menu_item_parent', '0'),
(128, 34, '_menu_item_object_id', '2'),
(129, 34, '_menu_item_object', 'page'),
(130, 34, '_menu_item_target', ''),
(131, 34, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(132, 34, '_menu_item_xfn', ''),
(133, 34, '_menu_item_url', ''),
(135, 35, '_menu_item_type', 'post_type'),
(136, 35, '_menu_item_menu_item_parent', '0'),
(137, 35, '_menu_item_object_id', '10'),
(138, 35, '_menu_item_object', 'page'),
(139, 35, '_menu_item_target', ''),
(140, 35, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(141, 35, '_menu_item_xfn', ''),
(142, 35, '_menu_item_url', ''),
(144, 36, '_menu_item_type', 'post_type'),
(145, 36, '_menu_item_menu_item_parent', '0'),
(146, 36, '_menu_item_object_id', '2'),
(147, 36, '_menu_item_object', 'page'),
(148, 36, '_menu_item_target', ''),
(149, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(150, 36, '_menu_item_xfn', ''),
(151, 36, '_menu_item_url', ''),
(153, 37, '_menu_item_type', 'post_type'),
(154, 37, '_menu_item_menu_item_parent', '0'),
(155, 37, '_menu_item_object_id', '10'),
(156, 37, '_menu_item_object', 'page'),
(157, 37, '_menu_item_target', ''),
(158, 37, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(159, 37, '_menu_item_xfn', ''),
(160, 37, '_menu_item_url', ''),
(162, 38, '_menu_item_type', 'post_type'),
(163, 38, '_menu_item_menu_item_parent', '0'),
(164, 38, '_menu_item_object_id', '2'),
(165, 38, '_menu_item_object', 'page'),
(166, 38, '_menu_item_target', '') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(167, 38, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(168, 38, '_menu_item_xfn', ''),
(169, 38, '_menu_item_url', ''),
(171, 40, '_edit_last', '1'),
(172, 40, '_edit_lock', '1541143456:1'),
(173, 41, '_dp_original', '40'),
(174, 42, '_dp_original', '40'),
(175, 43, '_dp_original', '40'),
(176, 41, '_edit_lock', '1541407183:1'),
(177, 41, '_edit_last', '1'),
(178, 42, '_edit_lock', '1541407028:1'),
(179, 42, '_edit_last', '1'),
(180, 43, '_edit_lock', '1541408820:1'),
(181, 43, '_edit_last', '1'),
(182, 44, '_edit_last', '1'),
(183, 44, '_edit_lock', '1541408626:1'),
(184, 40, 'date', '20190202'),
(185, 40, '_date', 'field_5bdbfbeb9a97a'),
(186, 40, 'place', 'Paris - Fr'),
(187, 40, '_place', 'field_5bdbfc0b9a97b'),
(188, 41, 'date', '20190118'),
(189, 41, '_date', 'field_5bdbfbeb9a97a'),
(190, 41, 'place', 'Paris - Fr'),
(191, 41, '_place', 'field_5bdbfc0b9a97b'),
(192, 42, 'date', '20181117'),
(193, 42, '_date', 'field_5bdbfbeb9a97a'),
(194, 42, 'place', 'Paris - Fr'),
(195, 42, '_place', 'field_5bdbfc0b9a97b'),
(196, 43, 'date', '20181122'),
(197, 43, '_date', 'field_5bdbfbeb9a97a'),
(198, 43, 'place', 'Paris - Fr'),
(199, 43, '_place', 'field_5bdbfc0b9a97b'),
(200, 47, '_menu_item_type', 'custom'),
(201, 47, '_menu_item_menu_item_parent', '0'),
(202, 47, '_menu_item_object_id', '47'),
(203, 47, '_menu_item_object', 'custom'),
(204, 47, '_menu_item_target', ''),
(205, 47, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(206, 47, '_menu_item_xfn', ''),
(207, 47, '_menu_item_url', '#'),
(209, 48, '_menu_item_type', 'custom'),
(210, 48, '_menu_item_menu_item_parent', '0'),
(211, 48, '_menu_item_object_id', '48'),
(212, 48, '_menu_item_object', 'custom'),
(213, 48, '_menu_item_target', ''),
(214, 48, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(215, 48, '_menu_item_xfn', ''),
(216, 48, '_menu_item_url', '#'),
(218, 49, '_menu_item_type', 'custom'),
(219, 49, '_menu_item_menu_item_parent', '0'),
(220, 49, '_menu_item_object_id', '49'),
(221, 49, '_menu_item_object', 'custom'),
(222, 49, '_menu_item_target', ''),
(223, 49, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(224, 49, '_menu_item_xfn', ''),
(225, 49, '_menu_item_url', '#'),
(227, 50, '_menu_item_type', 'custom'),
(228, 50, '_menu_item_menu_item_parent', '0'),
(229, 50, '_menu_item_object_id', '50'),
(230, 50, '_menu_item_object', 'custom'),
(231, 50, '_menu_item_target', ''),
(232, 50, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(233, 50, '_menu_item_xfn', ''),
(234, 50, '_menu_item_url', '#'),
(236, 51, '_menu_item_type', 'custom'),
(237, 51, '_menu_item_menu_item_parent', '0'),
(238, 51, '_menu_item_object_id', '51'),
(239, 51, '_menu_item_object', 'custom'),
(240, 51, '_menu_item_target', ''),
(241, 51, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(242, 51, '_menu_item_xfn', ''),
(243, 51, '_menu_item_url', '#'),
(244, 52, '_edit_last', '1'),
(245, 52, '_edit_lock', '1541575605:1'),
(246, 10, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(247, 10, '_slider_items', 'field_5bdfacdc7e99b'),
(248, 10, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(249, 10, '_slider_active', 'field_5bdfad6f09597'),
(250, 56, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(251, 56, '_slider_items', 'field_5bdfacdc7e99b'),
(252, 56, 'slider_active', ''),
(253, 56, '_slider_active', 'field_5bdfad6f09597'),
(272, 62, '_wp_attached_file', '2018/11/register.jpg'),
(273, 62, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:380;s:6:"height";i:575;s:4:"file";s:20:"2018/11/register.jpg";s:5:"sizes";a:11:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:20:"register-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_banner";a:4:{s:4:"file";s:20:"register-380x500.jpg";s:5:"width";i:380;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:16:"img_event_detail";a:4:{s:4:"file";s:20:"register-380x500.jpg";s:5:"width";i:380;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"register-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"register-198x300.jpg";s:5:"width";i:198;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:20:"register-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:20:"register-380x354.jpg";s:5:"width";i:380;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:20:"register-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:19:"register-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:20:"register-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:20:"register-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(274, 10, 'image_sidebar', '94'),
(275, 10, '_image_sidebar', 'field_5bdfafcc95a2b'),
(276, 10, 'image_sidebar_link', 'https://www.google.com'),
(277, 10, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(278, 63, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(279, 63, '_slider_items', 'field_5bdfacdc7e99b'),
(280, 63, 'slider_active', ''),
(281, 63, '_slider_active', 'field_5bdfad6f09597'),
(282, 63, 'image_sidebar', '62'),
(283, 63, '_image_sidebar', 'field_5bdfafcc95a2b'),
(284, 63, 'image_sidebar_link', 'https://www.google.com'),
(285, 63, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(286, 10, 'category_post', '13'),
(287, 10, '_category_post', 'field_5bdfc077457c8'),
(288, 10, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(289, 10, '_active_coingeek_week', 'field_5bdfc0aa457c9') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(290, 67, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(291, 67, '_slider_items', 'field_5bdfacdc7e99b'),
(292, 67, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(293, 67, '_slider_active', 'field_5bdfad6f09597'),
(294, 67, 'image_sidebar', '62'),
(295, 67, '_image_sidebar', 'field_5bdfafcc95a2b'),
(296, 67, 'image_sidebar_link', 'https://www.google.com'),
(297, 67, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(298, 67, 'category_post', '14'),
(299, 67, '_category_post', 'field_5bdfc077457c8'),
(300, 67, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(301, 67, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(302, 68, '_menu_item_type', 'custom'),
(303, 68, '_menu_item_menu_item_parent', '24'),
(304, 68, '_menu_item_object_id', '68'),
(305, 68, '_menu_item_object', 'custom'),
(306, 68, '_menu_item_target', ''),
(307, 68, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(308, 68, '_menu_item_xfn', ''),
(309, 68, '_menu_item_url', 'http://coingeek.me/news/'),
(311, 69, '_menu_item_type', 'custom'),
(312, 69, '_menu_item_menu_item_parent', '68'),
(313, 69, '_menu_item_object_id', '69'),
(314, 69, '_menu_item_object', 'custom'),
(315, 69, '_menu_item_target', ''),
(316, 69, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(317, 69, '_menu_item_xfn', ''),
(318, 69, '_menu_item_url', 'http://coingeek.me/news/'),
(320, 70, '_menu_item_type', 'custom'),
(321, 70, '_menu_item_menu_item_parent', '68'),
(322, 70, '_menu_item_object_id', '70'),
(323, 70, '_menu_item_object', 'custom'),
(324, 70, '_menu_item_target', ''),
(325, 70, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(326, 70, '_menu_item_xfn', ''),
(327, 70, '_menu_item_url', 'http://coingeek.me/new-category/business/'),
(329, 71, '_menu_item_type', 'custom'),
(330, 71, '_menu_item_menu_item_parent', '68'),
(331, 71, '_menu_item_object_id', '71'),
(332, 71, '_menu_item_object', 'custom'),
(333, 71, '_menu_item_target', ''),
(334, 71, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(335, 71, '_menu_item_xfn', ''),
(336, 71, '_menu_item_url', '#'),
(338, 72, '_menu_item_type', 'custom'),
(339, 72, '_menu_item_menu_item_parent', '68'),
(340, 72, '_menu_item_object_id', '72'),
(341, 72, '_menu_item_object', 'custom'),
(342, 72, '_menu_item_target', ''),
(343, 72, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(344, 72, '_menu_item_xfn', ''),
(345, 72, '_menu_item_url', '#'),
(347, 73, '_menu_item_type', 'custom'),
(348, 73, '_menu_item_menu_item_parent', '68'),
(349, 73, '_menu_item_object_id', '73'),
(350, 73, '_menu_item_object', 'custom'),
(351, 73, '_menu_item_target', ''),
(352, 73, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(353, 73, '_menu_item_xfn', ''),
(354, 73, '_menu_item_url', '#'),
(356, 74, '_menu_item_type', 'custom'),
(357, 74, '_menu_item_menu_item_parent', '68'),
(358, 74, '_menu_item_object_id', '74'),
(359, 74, '_menu_item_object', 'custom'),
(360, 74, '_menu_item_target', ''),
(361, 74, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(362, 74, '_menu_item_xfn', ''),
(363, 74, '_menu_item_url', '#'),
(365, 75, '_menu_item_type', 'custom'),
(366, 75, '_menu_item_menu_item_parent', '24'),
(367, 75, '_menu_item_object_id', '75'),
(368, 75, '_menu_item_object', 'custom'),
(369, 75, '_menu_item_target', ''),
(370, 75, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(371, 75, '_menu_item_xfn', ''),
(372, 75, '_menu_item_url', '#'),
(374, 1, '_edit_lock', '1541399131:1'),
(375, 76, '_edit_last', '1'),
(376, 76, '_edit_lock', '1541399526:1'),
(377, 81, '_wp_attached_file', '2018/11/image-programers.jpg'),
(378, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:895;s:6:"height";i:370;s:4:"file";s:28:"2018/11/image-programers.jpg";s:5:"sizes";a:10:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:28:"image-programers-280x370.jpg";s:5:"width";i:280;s:6:"height";i:370;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:28:"image-programers-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"image-programers-300x124.jpg";s:5:"width";i:300;s:6:"height";i:124;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"image-programers-768x317.jpg";s:5:"width";i:768;s:6:"height";i:317;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:28:"image-programers-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:28:"image-programers-680x354.jpg";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:28:"image-programers-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:27:"image-programers-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:28:"image-programers-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:28:"image-programers-280x370.jpg";s:5:"width";i:280;s:6:"height";i:370;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(379, 43, '_thumbnail_id', '81'),
(380, 43, 'website_link', 'https://codex.wordpress.org'),
(381, 43, '_website_link', 'field_5be00178f6df5'),
(382, 42, '_thumbnail_id', '81'),
(383, 42, 'website_link', 'https://codex.wordpress.org'),
(384, 42, '_website_link', 'field_5be00178f6df5'),
(385, 41, '_thumbnail_id', '81'),
(386, 41, 'website_link', 'https://codex.wordpress.org'),
(387, 41, '_website_link', 'field_5be00178f6df5'),
(388, 83, '_wp_attached_file', '2018/11/Screenshot_1.png'),
(389, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1860;s:6:"height";i:897;s:4:"file";s:24:"2018/11/Screenshot_1.png";s:5:"sizes";a:13:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:24:"Screenshot_1-280x460.png";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:9:"image/png";}s:14:"img_new_banner";a:4:{s:4:"file";s:25:"Screenshot_1-1860x500.png";s:5:"width";i:1860;s:6:"height";i:500;s:9:"mime-type";s:9:"image/png";}s:9:"thumbnail";a:4:{s:4:"file";s:24:"Screenshot_1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"Screenshot_1-300x145.png";s:5:"width";i:300;s:6:"height";i:145;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:24:"Screenshot_1-768x370.png";s:5:"width";i:768;s:6:"height";i:370;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"Screenshot_1-1024x494.png";s:5:"width";i:1024;s:6:"height";i:494;s:9:"mime-type";s:9:"image/png";}s:14:"img_new_normal";a:4:{s:4:"file";s:24:"Screenshot_1-380x188.png";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:9:"image/png";}s:15:"img_slider_home";a:4:{s:4:"file";s:24:"Screenshot_1-680x354.png";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:9:"image/png";}s:15:"img_latest_news";a:4:{s:4:"file";s:24:"Screenshot_1-280x139.png";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:9:"image/png";}s:18:"img_business_thumb";a:4:{s:4:"file";s:23:"Screenshot_1-120x80.png";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:9:"image/png";}s:12:"img_coingeek";a:4:{s:4:"file";s:24:"Screenshot_1-280x220.png";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:9:"image/png";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:24:"Screenshot_1-280x460.png";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:9:"image/png";}s:16:"img_event_detail";a:4:{s:4:"file";s:25:"Screenshot_1-1200x500.png";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(390, 84, '_edit_last', '1'),
(391, 84, '_edit_lock', '1541491169:1'),
(392, 14, 'template', '1'),
(393, 14, '_template', 'field_5be149a2c6927'),
(394, 86, '_wp_attached_file', '2018/11/DSC100312324.jpg'),
(395, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1080;s:4:"file";s:24:"2018/11/DSC100312324.jpg";s:5:"sizes";a:13:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:24:"DSC100312324-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_banner";a:4:{s:4:"file";s:25:"DSC100312324-1920x500.jpg";s:5:"width";i:1920;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:24:"DSC100312324-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"DSC100312324-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"DSC100312324-768x432.jpg";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:25:"DSC100312324-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:24:"DSC100312324-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:24:"DSC100312324-680x354.jpg";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:24:"DSC100312324-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:23:"DSC100312324-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:24:"DSC100312324-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:24:"DSC100312324-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:16:"img_event_detail";a:4:{s:4:"file";s:25:"DSC100312324-1200x500.jpg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(396, 17, 'template', '0'),
(397, 17, '_template', 'field_5be149a2c6927') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(398, 87, '_wp_attached_file', '2018/11/Wallpaper-v4-1920x1080.jpg'),
(399, 87, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1080;s:4:"file";s:34:"2018/11/Wallpaper-v4-1920x1080.jpg";s:5:"sizes";a:13:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_banner";a:4:{s:4:"file";s:35:"Wallpaper-v4-1920x1080-1920x500.jpg";s:5:"width";i:1920;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-768x432.jpg";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:35:"Wallpaper-v4-1920x1080-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-680x354.jpg";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:33:"Wallpaper-v4-1920x1080-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:34:"Wallpaper-v4-1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:16:"img_event_detail";a:4:{s:4:"file";s:35:"Wallpaper-v4-1920x1080-1200x500.jpg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(400, 16, 'template', '0'),
(401, 16, '_template', 'field_5be149a2c6927'),
(402, 88, '_wp_attached_file', '2018/11/Multi-Color-Wallpaper-17-1920x1080.jpg'),
(403, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1080;s:4:"file";s:46:"2018/11/Multi-Color-Wallpaper-17-1920x1080.jpg";s:5:"sizes";a:13:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_banner";a:4:{s:4:"file";s:47:"Multi-Color-Wallpaper-17-1920x1080-1920x500.jpg";s:5:"width";i:1920;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-768x432.jpg";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:47:"Multi-Color-Wallpaper-17-1920x1080-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-680x354.jpg";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:45:"Multi-Color-Wallpaper-17-1920x1080-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:46:"Multi-Color-Wallpaper-17-1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:16:"img_event_detail";a:4:{s:4:"file";s:47:"Multi-Color-Wallpaper-17-1920x1080-1200x500.jpg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(408, 91, '_wp_attached_file', '2018/11/Darksiders-Wrath-of-War_1920x1080.jpg'),
(409, 91, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1080;s:4:"file";s:45:"2018/11/Darksiders-Wrath-of-War_1920x1080.jpg";s:5:"sizes";a:13:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_banner";a:4:{s:4:"file";s:46:"Darksiders-Wrath-of-War_1920x1080-1920x500.jpg";s:5:"width";i:1920;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-300x169.jpg";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-768x432.jpg";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:46:"Darksiders-Wrath-of-War_1920x1080-1024x576.jpg";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-380x188.jpg";s:5:"width";i:380;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-680x354.jpg";s:5:"width";i:680;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:44:"Darksiders-Wrath-of-War_1920x1080-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:45:"Darksiders-Wrath-of-War_1920x1080-280x460.jpg";s:5:"width";i:280;s:6:"height";i:460;s:9:"mime-type";s:10:"image/jpeg";}s:16:"img_event_detail";a:4:{s:4:"file";s:46:"Darksiders-Wrath-of-War_1920x1080-1200x500.jpg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(410, 15, 'template', '0'),
(411, 15, '_template', 'field_5be149a2c6927'),
(412, 92, '_thumbnail_id', '86'),
(414, 92, 'template', '0'),
(415, 92, '_template', 'field_5be149a2c6927'),
(416, 92, '_dp_original', '17'),
(417, 93, '_thumbnail_id', '86'),
(418, 93, 'template', '0'),
(419, 93, '_template', 'field_5be149a2c6927'),
(421, 93, '_dp_original', '92'),
(422, 92, '_edit_lock', '1541661968:1'),
(423, 92, '_edit_last', '1'),
(424, 93, '_edit_lock', '1541671267:1'),
(425, 93, '_edit_last', '1'),
(426, 94, '_wp_attached_file', '2018/11/sidebar.jpg'),
(427, 94, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:281;s:6:"height";i:363;s:4:"file";s:19:"2018/11/sidebar.jpg";s:5:"sizes";a:9:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:19:"sidebar-280x363.jpg";s:5:"width";i:280;s:6:"height";i:363;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:19:"sidebar-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"sidebar-232x300.jpg";s:5:"width";i:232;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:19:"sidebar-281x188.jpg";s:5:"width";i:281;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:19:"sidebar-281x354.jpg";s:5:"width";i:281;s:6:"height";i:354;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:19:"sidebar-280x139.jpg";s:5:"width";i:280;s:6:"height";i:139;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:18:"sidebar-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:19:"sidebar-280x220.jpg";s:5:"width";i:280;s:6:"height";i:220;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:19:"sidebar-280x363.jpg";s:5:"width";i:280;s:6:"height";i:363;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(428, 95, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(429, 95, '_slider_items', 'field_5bdfacdc7e99b'),
(430, 95, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(431, 95, '_slider_active', 'field_5bdfad6f09597'),
(432, 95, 'image_sidebar', '94'),
(433, 95, '_image_sidebar', 'field_5bdfafcc95a2b'),
(434, 95, 'image_sidebar_link', 'https://www.google.com'),
(435, 95, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(436, 95, 'category_post', '14'),
(437, 95, '_category_post', 'field_5bdfc077457c8'),
(438, 95, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(439, 95, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(440, 101, '_wp_attached_file', '2018/11/banner_business.jpg'),
(441, 101, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1180;s:6:"height";i:133;s:4:"file";s:27:"2018/11/banner_business.jpg";s:5:"sizes";a:11:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:27:"banner_business-280x133.jpg";s:5:"width";i:280;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:27:"banner_business-150x133.jpg";s:5:"width";i:150;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"banner_business-300x34.jpg";s:5:"width";i:300;s:6:"height";i:34;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:26:"banner_business-768x87.jpg";s:5:"width";i:768;s:6:"height";i:87;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:28:"banner_business-1024x115.jpg";s:5:"width";i:1024;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:27:"banner_business-380x133.jpg";s:5:"width";i:380;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:27:"banner_business-680x133.jpg";s:5:"width";i:680;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:27:"banner_business-280x133.jpg";s:5:"width";i:280;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:26:"banner_business-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:27:"banner_business-280x133.jpg";s:5:"width";i:280;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:27:"banner_business-280x133.jpg";s:5:"width";i:280;s:6:"height";i:133;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(442, 102, '_wp_attached_file', '2018/11/banner_coin.jpg'),
(443, 102, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1180;s:6:"height";i:132;s:4:"file";s:23:"2018/11/banner_coin.jpg";s:5:"sizes";a:11:{s:15:"img_new_sidebar";a:4:{s:4:"file";s:23:"banner_coin-280x132.jpg";s:5:"width";i:280;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:23:"banner_coin-150x132.jpg";s:5:"width";i:150;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"banner_coin-300x34.jpg";s:5:"width";i:300;s:6:"height";i:34;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"banner_coin-768x86.jpg";s:5:"width";i:768;s:6:"height";i:86;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"banner_coin-1024x115.jpg";s:5:"width";i:1024;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}s:14:"img_new_normal";a:4:{s:4:"file";s:23:"banner_coin-380x132.jpg";s:5:"width";i:380;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_slider_home";a:4:{s:4:"file";s:23:"banner_coin-680x132.jpg";s:5:"width";i:680;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:15:"img_latest_news";a:4:{s:4:"file";s:23:"banner_coin-280x132.jpg";s:5:"width";i:280;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:22:"banner_coin-120x80.jpg";s:5:"width";i:120;s:6:"height";i:80;s:9:"mime-type";s:10:"image/jpeg";}s:12:"img_coingeek";a:4:{s:4:"file";s:23:"banner_coin-280x132.jpg";s:5:"width";i:280;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_coingeek_large";a:4:{s:4:"file";s:23:"banner_coin-280x132.jpg";s:5:"width";i:280;s:6:"height";i:132;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(444, 10, 'business_banner_image', '101'),
(445, 10, '_business_banner_image', 'field_5be2915e7c9a6'),
(446, 10, 'business_banner_link', 'https://www.google.com'),
(447, 10, '_business_banner_link', 'field_5be291797c9a7'),
(448, 10, 'coingeek_banner_image', '102'),
(449, 10, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(450, 10, 'coingeek_banner_link', 'https://www.google.com/'),
(451, 10, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(452, 103, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(453, 103, '_slider_items', 'field_5bdfacdc7e99b'),
(454, 103, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(455, 103, '_slider_active', 'field_5bdfad6f09597'),
(456, 103, 'image_sidebar', '94'),
(457, 103, '_image_sidebar', 'field_5bdfafcc95a2b'),
(458, 103, 'image_sidebar_link', 'https://www.google.com'),
(459, 103, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(460, 103, 'category_post', '14'),
(461, 103, '_category_post', 'field_5bdfc077457c8'),
(462, 103, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(463, 103, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(464, 103, 'business_banner_image', '101'),
(465, 103, '_business_banner_image', 'field_5be2915e7c9a6'),
(466, 103, 'business_banner_link', 'https://www.google.com'),
(467, 103, '_business_banner_link', 'field_5be291797c9a7'),
(468, 103, 'coingeek_banner_image', ''),
(469, 103, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(470, 103, 'coingeek_banner_link', ''),
(471, 103, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(472, 104, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(473, 104, '_slider_items', 'field_5bdfacdc7e99b'),
(474, 104, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(475, 104, '_slider_active', 'field_5bdfad6f09597'),
(476, 104, 'image_sidebar', '94'),
(477, 104, '_image_sidebar', 'field_5bdfafcc95a2b'),
(478, 104, 'image_sidebar_link', 'https://www.google.com'),
(479, 104, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(480, 104, 'category_post', '14'),
(481, 104, '_category_post', 'field_5bdfc077457c8'),
(482, 104, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(483, 104, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(484, 104, 'business_banner_image', '101'),
(485, 104, '_business_banner_image', 'field_5be2915e7c9a6'),
(486, 104, 'business_banner_link', 'https://www.google.com'),
(487, 104, '_business_banner_link', 'field_5be291797c9a7'),
(488, 104, 'coingeek_banner_image', '102'),
(489, 104, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(490, 104, 'coingeek_banner_link', 'https://www.google.com/'),
(491, 104, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(492, 108, '_wp_attached_file', '2018/11/AyreGroup-01.svg'),
(493, 109, '_wp_attached_file', '2018/11/AyreMedia-01.svg'),
(494, 110, '_wp_attached_file', '2018/11/CalvinAyre-01-2.svg'),
(495, 111, '_wp_attached_file', '2018/11/CalvinAyreFoundation-01.svg'),
(496, 10, 'business_articles', '11'),
(497, 10, '_business_articles', 'field_5be3b68b7e58e'),
(498, 10, 'technology_articles', '19'),
(499, 10, '_technology_articles', 'field_5be3b9da8f03b'),
(500, 10, 'video_articles', '16'),
(501, 10, '_video_articles', 'field_5be3b6df7e58f'),
(502, 119, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(503, 119, '_slider_items', 'field_5bdfacdc7e99b') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(504, 119, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(505, 119, '_slider_active', 'field_5bdfad6f09597'),
(506, 119, 'image_sidebar', '94'),
(507, 119, '_image_sidebar', 'field_5bdfafcc95a2b'),
(508, 119, 'image_sidebar_link', 'https://www.google.com'),
(509, 119, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(510, 119, 'category_post', '14'),
(511, 119, '_category_post', 'field_5bdfc077457c8'),
(512, 119, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(513, 119, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(514, 119, 'business_banner_image', '101'),
(515, 119, '_business_banner_image', 'field_5be2915e7c9a6'),
(516, 119, 'business_banner_link', 'https://www.google.com'),
(517, 119, '_business_banner_link', 'field_5be291797c9a7'),
(518, 119, 'coingeek_banner_image', '102'),
(519, 119, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(520, 119, 'coingeek_banner_link', 'https://www.google.com/'),
(521, 119, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(522, 119, 'business_articles', '11'),
(523, 119, '_business_articles', 'field_5be3b68b7e58e'),
(524, 119, 'technology_articles', '13'),
(525, 119, '_technology_articles', 'field_5be3b9da8f03b'),
(526, 119, 'video_articles', '16'),
(527, 119, '_video_articles', 'field_5be3b6df7e58f'),
(528, 120, '_edit_last', '1'),
(529, 120, '_edit_lock', '1541653555:1'),
(530, 120, '_thumbnail_id', '88'),
(531, 120, 'template', '0'),
(532, 120, '_template', 'field_5be149a2c6927'),
(533, 121, '_thumbnail_id', '88'),
(534, 121, 'template', '0'),
(535, 121, '_template', 'field_5be149a2c6927'),
(536, 121, '_dp_original', '120'),
(537, 122, '_thumbnail_id', '88'),
(538, 122, 'template', '0'),
(539, 122, '_template', 'field_5be149a2c6927'),
(540, 122, '_dp_original', '120'),
(541, 121, '_edit_lock', '1541654137:1'),
(542, 121, '_edit_last', '1'),
(543, 122, '_edit_lock', '1541654195:1'),
(544, 122, '_edit_last', '1'),
(545, 123, '_edit_last', '1'),
(546, 123, '_edit_lock', '1541654406:1'),
(547, 123, 'template', '0'),
(548, 123, '_template', 'field_5be149a2c6927'),
(549, 123, '_thumbnail_id', '87'),
(550, 124, 'template', '0'),
(551, 124, '_template', 'field_5be149a2c6927'),
(552, 124, '_thumbnail_id', '87'),
(553, 124, '_dp_original', '123'),
(554, 125, '_thumbnail_id', '88'),
(555, 125, 'template', '0'),
(556, 125, '_template', 'field_5be149a2c6927'),
(558, 125, '_dp_original', '122'),
(559, 126, '_thumbnail_id', '88'),
(560, 126, 'template', '0'),
(561, 126, '_template', 'field_5be149a2c6927'),
(563, 126, '_dp_original', '125'),
(564, 125, '_edit_lock', '1541661498:1'),
(565, 126, '_edit_lock', '1541661498:1'),
(566, 125, '_edit_last', '1'),
(567, 126, '_edit_last', '1'),
(568, 127, '_thumbnail_id', '88'),
(569, 127, 'template', '0'),
(570, 127, '_template', 'field_5be149a2c6927'),
(571, 128, '_thumbnail_id', '88'),
(573, 128, 'template', '0'),
(574, 128, '_template', 'field_5be149a2c6927'),
(576, 127, '_dp_original', '126'),
(577, 128, '_dp_original', '126'),
(578, 127, '_edit_lock', '1541661675:1'),
(579, 127, '_edit_last', '1'),
(580, 128, '_edit_lock', '1541661711:1'),
(581, 128, '_edit_last', '1'),
(582, 129, 'template', '0'),
(583, 129, '_template', 'field_5be149a2c6927'),
(584, 129, '_thumbnail_id', '87'),
(586, 129, '_dp_original', '124'),
(587, 130, 'template', '0'),
(588, 130, '_template', 'field_5be149a2c6927'),
(589, 130, '_thumbnail_id', '87'),
(591, 130, '_dp_original', '124'),
(592, 131, 'template', '0'),
(593, 131, '_template', 'field_5be149a2c6927'),
(594, 131, '_thumbnail_id', '87'),
(596, 131, '_dp_original', '124'),
(597, 132, 'template', '0'),
(598, 132, '_template', 'field_5be149a2c6927'),
(599, 132, '_thumbnail_id', '87'),
(601, 132, '_dp_original', '124'),
(602, 124, '_edit_lock', '1541661777:1'),
(603, 124, '_edit_last', '1'),
(604, 129, '_edit_lock', '1541661791:1'),
(605, 129, '_edit_last', '1'),
(606, 132, '_edit_lock', '1541661821:1'),
(607, 132, '_edit_last', '1'),
(608, 131, '_edit_lock', '1541661835:1'),
(609, 131, '_edit_last', '1'),
(610, 130, '_edit_lock', '1541661847:1'),
(611, 130, '_edit_last', '1') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(612, 133, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(613, 133, '_slider_items', 'field_5bdfacdc7e99b'),
(614, 133, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(615, 133, '_slider_active', 'field_5bdfad6f09597'),
(616, 133, 'image_sidebar', '94'),
(617, 133, '_image_sidebar', 'field_5bdfafcc95a2b'),
(618, 133, 'image_sidebar_link', 'https://www.google.com'),
(619, 133, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(620, 133, 'category_post', '14'),
(621, 133, '_category_post', 'field_5bdfc077457c8'),
(622, 133, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(623, 133, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(624, 133, 'business_banner_image', '101'),
(625, 133, '_business_banner_image', 'field_5be2915e7c9a6'),
(626, 133, 'business_banner_link', 'https://www.google.com'),
(627, 133, '_business_banner_link', 'field_5be291797c9a7'),
(628, 133, 'coingeek_banner_image', '102'),
(629, 133, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(630, 133, 'coingeek_banner_link', 'https://www.google.com/'),
(631, 133, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(632, 133, 'business_articles', '11'),
(633, 133, '_business_articles', 'field_5be3b68b7e58e'),
(634, 133, 'technology_articles', '19'),
(635, 133, '_technology_articles', 'field_5be3b9da8f03b'),
(636, 133, 'video_articles', '16'),
(637, 133, '_video_articles', 'field_5be3b6df7e58f'),
(638, 134, 'template', '0'),
(639, 134, '_template', 'field_5be149a2c6927'),
(640, 134, '_thumbnail_id', '87'),
(642, 134, '_dp_original', '130'),
(643, 135, 'template', '0'),
(644, 135, '_template', 'field_5be149a2c6927'),
(645, 135, '_thumbnail_id', '87'),
(647, 135, '_dp_original', '134'),
(648, 136, 'template', '0'),
(649, 136, '_template', 'field_5be149a2c6927'),
(650, 136, '_thumbnail_id', '87'),
(652, 136, '_dp_original', '134'),
(653, 137, 'template', '0'),
(654, 137, '_template', 'field_5be149a2c6927'),
(655, 137, '_thumbnail_id', '87'),
(657, 137, '_dp_original', '134'),
(658, 138, 'template', '0'),
(659, 138, '_template', 'field_5be149a2c6927'),
(660, 138, '_thumbnail_id', '87'),
(662, 138, '_dp_original', '134'),
(663, 139, 'template', '0'),
(664, 139, '_template', 'field_5be149a2c6927'),
(665, 139, '_thumbnail_id', '87'),
(667, 139, '_dp_original', '135'),
(668, 134, '_edit_lock', '1541662135:1'),
(669, 134, '_edit_last', '1'),
(670, 139, '_edit_lock', '1541662153:1'),
(671, 139, '_edit_last', '1'),
(672, 138, '_edit_lock', '1541662167:1'),
(673, 138, '_edit_last', '1'),
(674, 137, '_edit_lock', '1541662189:1'),
(675, 137, '_edit_last', '1'),
(676, 136, '_edit_lock', '1541662203:1'),
(677, 136, '_edit_last', '1'),
(678, 135, '_edit_lock', '1541662239:1'),
(679, 135, '_edit_last', '1'),
(680, 140, 'slider_items', 'a:3:{i:0;s:2:"15";i:1;s:2:"17";i:2;s:2:"16";}'),
(681, 140, '_slider_items', 'field_5bdfacdc7e99b'),
(682, 140, 'slider_active', 'a:1:{i:0;s:1:"1";}'),
(683, 140, '_slider_active', 'field_5bdfad6f09597'),
(684, 140, 'image_sidebar', '94'),
(685, 140, '_image_sidebar', 'field_5bdfafcc95a2b'),
(686, 140, 'image_sidebar_link', 'https://www.google.com'),
(687, 140, '_image_sidebar_link', 'field_5bdfaffb95a2c'),
(688, 140, 'category_post', '13'),
(689, 140, '_category_post', 'field_5bdfc077457c8'),
(690, 140, 'active_coingeek_week', 'a:1:{i:0;s:1:"1";}'),
(691, 140, '_active_coingeek_week', 'field_5bdfc0aa457c9'),
(692, 140, 'business_banner_image', '101'),
(693, 140, '_business_banner_image', 'field_5be2915e7c9a6'),
(694, 140, 'business_banner_link', 'https://www.google.com'),
(695, 140, '_business_banner_link', 'field_5be291797c9a7'),
(696, 140, 'coingeek_banner_image', '102'),
(697, 140, '_coingeek_banner_image', 'field_5be28a5d13d5d'),
(698, 140, 'coingeek_banner_link', 'https://www.google.com/'),
(699, 140, '_coingeek_banner_link', 'field_5be28a7f13d5e'),
(700, 140, 'business_articles', '11'),
(701, 140, '_business_articles', 'field_5be3b68b7e58e'),
(702, 140, 'technology_articles', '19'),
(703, 140, '_technology_articles', 'field_5be3b9da8f03b'),
(704, 140, 'video_articles', '16'),
(705, 140, '_video_articles', 'field_5be3b6df7e58f'),
(706, 142, '_wp_attached_file', '2018/11/lg.jpg'),
(707, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:154;s:6:"height";i:34;s:4:"file";s:14:"2018/11/lg.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"lg-150x34.jpg";s:5:"width";i:150;s:6:"height";i:34;s:9:"mime-type";s:10:"image/jpeg";}s:18:"img_business_thumb";a:4:{s:4:"file";s:13:"lg-120x34.jpg";s:5:"width";i:120;s:6:"height";i:34;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-11-01 05:47:01', '2018-11-01 04:47:01', 'Bienvenue sur WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis lancez-vous !', 'Bonjour tout le monde !', '', 'publish', 'open', 'open', '', 'bonjour-tout-le-monde', '', '', '2018-11-01 05:47:01', '2018-11-01 04:47:01', '', 0, 'http://coingeek.me/?p=1', 0, 'post', '', 1),
(2, 1, '2018-11-01 05:47:01', '2018-11-01 04:47:01', 'Voici un exemple de page. Elle est différente d’un article de blog, en cela qu’elle restera à la même place, et s’affichera dans le menu de navigation de votre site (en fonction de votre thème). La plupart des gens commencent par écrire une page « À Propos » qui les présente aux visiteurs potentiels du site. Vous pourriez y écrire quelque chose de ce tenant :\n\n<blockquote>Bonjour ! Je suis un mécanicien qui aspire à devenir un acteur, et ceci est mon blog. J’habite à Bordeaux, j’ai un super chien qui s’appelle Russell, et j’aime la vodka-ananas (ainsi que perdre mon temps à regarder la pluie tomber).</blockquote>\n\n...ou bien quelque chose comme cela :\n\n<blockquote>La société 123 Machin Truc a été créée en 1971, et n’a cessé de proposer au public des machins-trucs de qualité depuis cette année. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</blockquote>\n\nÉtant donné que vous êtes un nouvel utilisateur ou une nouvelle utilisatrice de WordPress, vous devriez vous rendre sur votre <a href="http://coingeek.me/wp-admin/">tableau de bord</a> pour effacer la présente page, et créer de nouvelles pages avec votre propre contenu. Amusez-vous bien !', 'Page d’exemple', '', 'publish', 'closed', 'open', '', 'page-d-exemple', '', '', '2018-11-01 05:47:01', '2018-11-01 04:47:01', '', 0, 'http://coingeek.me/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-11-01 05:47:01', '2018-11-01 04:47:01', '<h2>Qui sommes-nous ?</h2><p>L’adresse de notre site Web est : http://coingeek.me.</p><h2>Utilisation des données personnelles collectées</h2><h3>Commentaires</h3><p>Quand vous laissez un commentaire sur notre site web, les données inscrites dans le formulaire de commentaire, mais aussi votre adresse IP et l’agent utilisateur de votre navigateur sont collectés pour nous aider à la détection des commentaires indésirables.</p><p>Une chaîne anonymisée créée à partir de votre adresse de messagerie (également appelée hash) peut être envoyée au service Gravatar pour vérifier si vous utilisez ce dernier. Les clauses de confidentialité du service Gravatar sont disponibles ici : https://automattic.com/privacy/. Après validation de votre commentaire, votre photo de profil sera visible publiquement à coté de votre commentaire.</p><h3>Médias</h3><p>Si vous êtes un utilisateur ou une utilisatrice enregistré·e et que vous téléversez des images sur le site web, nous vous conseillons d’éviter de téléverser des images contenant des données EXIF de coordonnées GPS. Les visiteurs de votre site web peuvent télécharger et extraire des données de localisation depuis ces images.</p><h3>Formulaires de contact</h3><h3>Cookies</h3><p>Si vous déposez un commentaire sur notre site, il vous sera proposé d’enregistrer votre nom, adresse de messagerie et site web dans des cookies. C’est uniquement pour votre confort afin de ne pas avoir à saisir ces informations si vous déposez un autre commentaire plus tard. Ces cookies expirent au bout d’un an.</p><p>Si vous avez un compte et que vous vous connectez sur ce site, un cookie temporaire sera créé afin de déterminer si votre navigateur accepte les cookies. Il ne contient pas de données personnelles et sera supprimé automatiquement à la fermeture de votre navigateur.</p><p>Lorsque vous vous connecterez, nous mettrons en place un certain nombre de cookies pour enregistrer vos informations de connexion et vos préférences d’écran. La durée de vie d’un cookie de connexion est de deux jours, celle d’un cookie d’option d’écran est d’un an. Si vous cochez « Se souvenir de moi », votre cookie de connexion sera conservé pendant deux semaines. Si vous vous déconnectez de votre compte, le cookie de connexion sera effacé.</p><p>En modifiant ou en publiant une publication, un cookie supplémentaire sera enregistré dans votre navigateur. Ce cookie ne comprend aucune donnée personnelle. Il indique simplement l’ID de la publication que vous venez de modifier. Il expire au bout d’un jour.</p><h3>Contenu embarqué depuis d’autres sites</h3><p>Les articles de ce site peuvent inclure des contenus intégrés (par exemple des vidéos, images, articles…). Le contenu intégré depuis d’autres sites se comporte de la même manière que si le visiteur se rendait sur cet autre site.</p><p>Ces sites web pourraient collecter des données sur vous, utiliser des cookies, embarquer des outils de suivis tiers, suivre vos interactions avec ces contenus embarqués si vous disposez d’un compte connecté sur leur site web.</p><h3>Statistiques et mesures d’audience</h3><h2>Utilisation et transmission de vos données personnelles</h2><h2>Durées de stockage de vos données</h2><p>Si vous laissez un commentaire, le commentaire et ses métadonnées sont conservés indéfiniment. Cela permet de reconnaître et approuver automatiquement les commentaires suivants au lieu de les laisser dans la file de modération.</p><p>Pour les utilisateurs et utilisatrices qui s’enregistrent sur notre site (si cela est possible), nous stockons également les données personnelles indiquées dans leur profil. Tous les utilisateurs et utilisatrices peuvent voir, modifier ou supprimer leurs informations personnelles à tout moment (à l’exception de leur nom d’utilisateur·ice). Les gestionnaires du site peuvent aussi voir et modifier ces informations.</p><h2>Les droits que vous avez sur vos données</h2><p>Si vous avez un compte ou si vous avez laissé des commentaires sur le site, vous pouvez demander à recevoir un fichier contenant toutes les données personnelles que nous possédons à votre sujet, incluant celles que vous nous avez fournies. Vous pouvez également demander la suppression des données personnelles vous concernant. Cela ne prend pas en compte les données stockées à des fins administratives, légales ou pour des raisons de sécurité.</p><h2>Transmission de vos données personnelles</h2><p>Les commentaires des visiteurs peuvent être vérifiés à l’aide d’un service automatisé de détection des commentaires indésirables.</p><h2>Informations de contact</h2><h2>Informations supplémentaires</h2><h3>Comment nous protégeons vos données</h3><h3>Procédures mises en œuvre en cas de fuite de données</h3><h3>Les services tiers qui nous transmettent des données</h3><h3>Opérations de marketing automatisé et/ou de profilage réalisées à l’aide des données personnelles</h3><h3>Affichage des informations liées aux secteurs soumis à des régulations spécifiques</h3>', 'Politique de confidentialité', '', 'draft', 'closed', 'open', '', 'politique-de-confidentialite', '', '', '2018-11-01 05:47:01', '2018-11-01 04:47:01', '', 0, 'http://coingeek.me/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-11-01 09:06:45', '2018-11-01 08:06:45', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-11-01 09:06:45', '2018-11-01 08:06:45', '', 0, 'http://coingeek.me/wp-content/uploads/2018/11/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(6, 1, '2018-11-01 09:07:02', '2018-11-01 08:07:02', 'a:8:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:21:"athena-theme-settings";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";s:5:"local";s:3:"php";}', 'Réglages du Site', 'reglages-du-site', 'publish', 'closed', 'closed', '', 'group_5bce6c1b9acc8', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 0, 'http://coingeek.me/?post_type=acf-field-group&#038;p=6', 0, 'acf-field-group', '', 0),
(7, 1, '2018-11-01 09:07:02', '2018-11-01 08:07:02', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Header', '', 'publish', 'closed', 'closed', '', 'field_5bce6c2d063ed', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://coingeek.me/?post_type=acf-field&#038;p=7', 0, 'acf-field', '', 0),
(8, 1, '2018-11-01 09:07:02', '2018-11-01 08:07:02', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Logo', 'at_logo', 'publish', 'closed', 'closed', '', 'field_5bce6c3b063ee', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://coingeek.me/?post_type=acf-field&#038;p=8', 1, 'acf-field', '', 0),
(10, 1, '2018-11-01 09:18:45', '2018-11-01 08:18:45', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-11-08 08:33:29', '2018-11-08 07:33:29', '', 0, 'http://coingeek.me/?page_id=10', 0, 'page', '', 0),
(11, 1, '2018-11-01 09:18:45', '2018-11-01 08:18:45', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-01 09:18:45', '2018-11-01 08:18:45', '', 10, 'http://coingeek.me/2018/11/01/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2018-11-01 09:40:21', '2018-11-01 08:40:21', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Test 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-1', '', '', '2018-11-01 09:43:30', '2018-11-01 08:43:30', '', 0, 'http://coingeek.me/?post_type=actualites&#038;p=12', 0, 'actualites', '', 0),
(13, 1, '2018-11-01 09:43:13', '2018-11-01 08:43:13', '', 'item-home', '', 'inherit', 'open', 'closed', '', 'item-home', '', '', '2018-11-01 09:43:13', '2018-11-01 08:43:13', '', 12, 'http://coingeek.me/wp-content/uploads/2018/11/item-home.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2018-11-01 09:46:27', '2018-11-01 08:46:27', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Business 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-2', '', '', '2018-11-08 08:27:20', '2018-11-08 07:27:20', '', 0, 'http://coingeek.me/?post_type=actualites&#038;p=14', 0, 'news', '', 3),
(15, 1, '2018-11-01 09:46:16', '2018-11-01 08:46:16', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Business 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-3', '', '', '2018-11-08 08:26:56', '2018-11-08 07:26:56', '', 0, 'http://coingeek.me/?post_type=actualites&#038;p=15', 0, 'news', '', 0),
(16, 1, '2018-11-01 09:46:40', '2018-11-01 08:46:40', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n<img class="wp-image-83 size-full aligncenter" src="http://coingeek.me/wp-content/uploads/2018/11/Screenshot_1.png" alt="" width="1860" height="897" />\r\n\r\nWhen Brazilian-born Henrique Dubugras and Pedro Franceschi met at 16 years old, they bonded over a love of coding and mutual frustrations with their strict mothers, who didn’t understand their Mark Zuckerberg-esque ambitions.\r\n\r\nTo be fair, their moms’ fear of their hacking habits only escalated after their pre-teen sons received legal notices of patent infringements in the mail. A legal threat from Apple, which Franceschi received after discovering the first jailbreak to the iPhone, is enough to warrant a grounding, at the very least.\r\n\r\nTheir parents implored them to quit the hacking and stop messing around online.\r\n\r\nThey didn’t listen.\r\n\r\nToday, the now 22-year-olds are announcing a $125 million Series C for their second successful payments business, called Brex, at a $1.1 billion valuation. Greenoaks Capital, DST Global and IVP led the round, which brings their total raised to date to about $200 million.\r\n\r\nToday, the now 22-year-olds are announcing a $125 million Series C for their second successful payments business, called Brex, at a $1.1 billion valuation. Greenoaks Capital, DST Global and IVP led the round, which brings their total raised to date to about $200 million.\r\n\r\nSan Francisco-based Brex provides startup founders access to corporate credit cards without a personal guarantee or deposit. It’s also supported by the likes of PayPal founders Peter Thiel and Max Levchin, the former chief executive officer of Visa Carl Pascarella and a handful of leading venture capital firms.', 'Business 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-4', '', '', '2018-11-08 08:27:42', '2018-11-08 07:27:42', '', 0, 'http://coingeek.me/?post_type=actualites&#038;p=16', 0, 'news', '', 2),
(17, 1, '2018-11-01 09:46:54', '2018-11-01 08:46:54', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Business 5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-5', '', '', '2018-11-08 08:28:02', '2018-11-08 07:28:02', '', 0, 'http://coingeek.me/?post_type=actualites&#038;p=17', 0, 'news', '', 5),
(18, 1, '2018-11-01 11:06:47', '2018-11-01 10:06:47', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Footer', '', 'publish', 'closed', 'closed', '', 'field_5bdad00a9af16', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://coingeek.me/?post_type=acf-field&#038;p=18', 3, 'acf-field', '', 0),
(19, 1, '2018-11-01 11:06:47', '2018-11-01 10:06:47', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Footer Logo', 'footer_logo', 'publish', 'closed', 'closed', '', 'field_5bdad01a9af17', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://coingeek.me/?post_type=acf-field&#038;p=19', 4, 'acf-field', '', 0),
(20, 1, '2018-11-01 11:07:09', '2018-11-01 10:07:09', '', 'Coingeek', '', 'inherit', 'open', 'closed', '', 'coingeek', '', '', '2018-11-01 11:07:09', '2018-11-01 10:07:09', '', 0, 'http://coingeek.me/wp-content/uploads/2018/11/Coingeek.svg', 0, 'attachment', 'image/svg+xml', 0),
(21, 1, '2018-11-01 11:13:11', '2018-11-01 10:13:11', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";i:0;s:3:"max";i:0;s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Socials', 'socials', 'publish', 'closed', 'closed', '', 'field_5bdad1b13543a', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://coingeek.me/?post_type=acf-field&#038;p=21', 2, 'acf-field', '', 0),
(22, 1, '2018-11-01 11:14:51', '2018-11-01 10:14:51', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Social link', 'social_link', 'publish', 'closed', 'closed', '', 'field_5bdad1ea5ce2b', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 21, 'http://coingeek.me/?post_type=acf-field&#038;p=22', 0, 'acf-field', '', 0),
(23, 1, '2018-11-01 11:14:51', '2018-11-01 10:14:51', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Social Class', 'social_class', 'publish', 'closed', 'closed', '', 'field_5bdad1fc5ce2c', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 21, 'http://coingeek.me/?post_type=acf-field&#038;p=23', 1, 'acf-field', '', 0),
(24, 1, '2018-11-01 11:26:27', '2018-11-01 10:26:27', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=24', 1, 'nav_menu_item', '', 0),
(31, 1, '2018-11-01 12:05:15', '2018-11-01 11:05:15', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2018-11-01 12:05:15', '2018-11-01 11:05:15', '', 0, 'http://coingeek.me/?p=31', 1, 'nav_menu_item', '', 0),
(32, 1, '2018-11-01 12:05:15', '2018-11-01 11:05:15', ' ', '', '', 'publish', 'closed', 'closed', '', '32', '', '', '2018-11-01 12:05:15', '2018-11-01 11:05:15', '', 0, 'http://coingeek.me/?p=32', 2, 'nav_menu_item', '', 0),
(33, 1, '2018-11-01 12:05:15', '2018-11-01 11:05:15', ' ', '', '', 'publish', 'closed', 'closed', '', '33', '', '', '2018-11-01 12:05:15', '2018-11-01 11:05:15', '', 0, 'http://coingeek.me/?p=33', 3, 'nav_menu_item', '', 0),
(34, 1, '2018-11-01 12:05:15', '2018-11-01 11:05:15', ' ', '', '', 'publish', 'closed', 'closed', '', '34', '', '', '2018-11-01 12:05:15', '2018-11-01 11:05:15', '', 0, 'http://coingeek.me/?p=34', 4, 'nav_menu_item', '', 0),
(35, 1, '2018-11-01 12:05:35', '2018-11-01 11:05:35', ' ', '', '', 'publish', 'closed', 'closed', '', '35', '', '', '2018-11-01 12:06:29', '2018-11-01 11:06:29', '', 0, 'http://coingeek.me/?p=35', 1, 'nav_menu_item', '', 0),
(36, 1, '2018-11-01 12:05:35', '2018-11-01 11:05:35', ' ', '', '', 'publish', 'closed', 'closed', '', '36', '', '', '2018-11-01 12:06:29', '2018-11-01 11:06:29', '', 0, 'http://coingeek.me/?p=36', 2, 'nav_menu_item', '', 0),
(37, 1, '2018-11-01 12:05:36', '2018-11-01 11:05:36', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2018-11-01 12:06:29', '2018-11-01 11:06:29', '', 0, 'http://coingeek.me/?p=37', 3, 'nav_menu_item', '', 0),
(38, 1, '2018-11-01 12:05:36', '2018-11-01 11:05:36', ' ', '', '', 'publish', 'closed', 'closed', '', '38', '', '', '2018-11-01 12:06:29', '2018-11-01 11:06:29', '', 0, 'http://coingeek.me/?p=38', 4, 'nav_menu_item', '', 0),
(39, 1, '2018-11-02 03:29:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-11-02 03:29:17', '0000-00-00 00:00:00', '', 0, 'http://coingeek.me/?post_type=news&p=39', 0, 'news', '', 0),
(40, 1, '2018-11-02 04:47:45', '2018-11-02 03:47:45', '<div class="post-block">\r\n<div class="row">\r\n<div class="img-col">\r\n<div class="img-wrapper"><span class="img"><img class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg" sizes="(max-width: 730px) 100vw, 730px" srcset="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg 730w, https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018-300x148.jpg 300w" alt="World Crypto Conference" width="730" height="360" /></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="post-content post-indent">\r\n\r\n<strong>The World’s Largest Crypto Conference</strong>\r\n\r\nOver 3,000 Attendees, 150+ Exhibitors and 200,000 sq ft of meeting &amp; exhibit space.\r\n\r\nTOP KEYNOTE SPEAKERS, NON-STOP EDUCATION AND NETWORKING OPPORTUNITIES.\r\n\r\nYou Don’t Want to Miss This Crypto Conference!\r\n\r\nHosted by Travis Wright and Joel Comm of the Bad Crypto Podcast\r\n\r\n<a href="https://worldcryptocon.com/" target="_blank" rel="nofollow noopener">World Crypto Con</a> will be hosted by Travis Wright and Joel Comm. Hosts of the worlds #1 Crypto Podcast (The Bad Crypto Podcast). With over 2.5 million downloads and growing, Joel and Travis have become the go to podcast for Crypto enthusiasts around the globe. Stay Bad!\r\n\r\n</div>', 'World Crypto Conference', '', 'publish', 'closed', 'closed', '', 'world-crypto-conference', '', '', '2018-11-02 08:26:38', '2018-11-02 07:26:38', '', 0, 'http://coingeek.me/?post_type=event&#038;p=40', 0, 'event', '', 0),
(41, 1, '2018-11-02 04:48:31', '2018-11-02 03:48:31', '<div class="post-block">\r\n<div class="row">\r\n<div class="img-col">\r\n<div class="img-wrapper"><span class="img"><img class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg" sizes="(max-width: 730px) 100vw, 730px" srcset="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg 730w, https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018-300x148.jpg 300w" alt="World Crypto Conference" width="730" height="360" /></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="post-content post-indent">\r\n\r\n<strong>The World’s Largest Crypto Conference</strong>\r\n\r\nOver 3,000 Attendees, 150+ Exhibitors and 200,000 sq ft of meeting &amp; exhibit space.\r\n\r\nTOP KEYNOTE SPEAKERS, NON-STOP EDUCATION AND NETWORKING OPPORTUNITIES.\r\n\r\nYou Don’t Want to Miss This Crypto Conference!\r\n\r\nHosted by Travis Wright and Joel Comm of the Bad Crypto Podcast\r\n\r\n<a href="https://worldcryptocon.com/" target="_blank" rel="nofollow noopener">World Crypto Con</a> will be hosted by Travis Wright and Joel Comm. Hosts of the worlds #1 Crypto Podcast (The Bad Crypto Podcast). With over 2.5 million downloads and growing, Joel and Travis have become the go to podcast for Crypto enthusiasts around the globe. Stay Bad!\r\n\r\n</div>', 'World Crypto Conference 1', '', 'publish', 'closed', 'closed', '', 'world-crypto-conference-1', '', '', '2018-11-05 09:39:43', '2018-11-05 08:39:43', '', 0, 'http://coingeek.me/?post_type=event&#038;p=41', 0, 'event', '', 0),
(42, 1, '2018-11-02 04:48:42', '2018-11-02 03:48:42', '<div class="post-block">\r\n<div class="row">\r\n<div class="img-col">\r\n<div class="img-wrapper"><span class="img"><img class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg" sizes="(max-width: 730px) 100vw, 730px" srcset="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg 730w, https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018-300x148.jpg 300w" alt="World Crypto Conference" width="730" height="360" /></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="post-content post-indent">\r\n\r\n<strong>The World’s Largest Crypto Conference</strong>\r\n\r\nOver 3,000 Attendees, 150+ Exhibitors and 200,000 sq ft of meeting &amp; exhibit space.\r\n\r\nTOP KEYNOTE SPEAKERS, NON-STOP EDUCATION AND NETWORKING OPPORTUNITIES.\r\n\r\nYou Don’t Want to Miss This Crypto Conference!\r\n\r\nHosted by Travis Wright and Joel Comm of the Bad Crypto Podcast\r\n\r\n<a href="https://worldcryptocon.com/" target="_blank" rel="nofollow noopener">World Crypto Con</a> will be hosted by Travis Wright and Joel Comm. Hosts of the worlds #1 Crypto Podcast (The Bad Crypto Podcast). With over 2.5 million downloads and growing, Joel and Travis have become the go to podcast for Crypto enthusiasts around the globe. Stay Bad!\r\n\r\n</div>', 'World Crypto Conference 2', '', 'publish', 'closed', 'closed', '', 'world-crypto-conference-2', '', '', '2018-11-05 09:39:29', '2018-11-05 08:39:29', '', 0, 'http://coingeek.me/?post_type=event&#038;p=42', 0, 'event', '', 0),
(43, 1, '2018-11-02 04:48:51', '2018-11-02 03:48:51', '<div class="post-block">\r\n<div class="row">\r\n<div class="img-col">\r\n<div class="img-wrapper"><span class="img"><img class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg" sizes="(max-width: 730px) 100vw, 730px" srcset="https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018.jpg 730w, https://coingeek.com/app/uploads/2018/04/world-crypto-conference-2018-300x148.jpg 300w" alt="World Crypto Conference" width="730" height="360" /></span></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="post-content post-indent">\r\n\r\n<strong>The World’s Largest Crypto Conference</strong>\r\n\r\nOver 3,000 Attendees, 150+ Exhibitors and 200,000 sq ft of meeting &amp; exhibit space.\r\n\r\nTOP KEYNOTE SPEAKERS, NON-STOP EDUCATION AND NETWORKING OPPORTUNITIES.\r\n\r\nYou Don’t Want to Miss This Crypto Conference!\r\n\r\nHosted by Travis Wright and Joel Comm of the Bad Crypto Podcast\r\n<blockquote><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard</blockquote>\r\n<a href="https://worldcryptocon.com/" target="_blank" rel="nofollow noopener">World Crypto Con</a> will be hosted by Travis Wright and Joel Comm. Hosts of the worlds #1 Crypto Podcast (The Bad Crypto Podcast). With over 2.5 million downloads and growing, Joel and Travis have become the go to podcast for Crypto enthusiasts around the globe. Stay Bad!\r\n\r\n</div>', 'World Crypto Conference 3', '', 'publish', 'closed', 'closed', '', 'world-crypto-conference-3', '', '', '2018-11-05 10:06:59', '2018-11-05 09:06:59', '', 0, 'http://coingeek.me/?post_type=event&#038;p=43', 0, 'event', '', 0),
(44, 1, '2018-11-02 08:26:10', '2018-11-02 07:26:10', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:5:"event";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Events', 'events', 'publish', 'closed', 'closed', '', 'group_5bdbfbe2352d9', '', '', '2018-11-05 09:38:25', '2018-11-05 08:38:25', '', 0, 'http://coingeek.me/?post_type=acf-field-group&#038;p=44', 0, 'acf-field-group', '', 0),
(45, 1, '2018-11-02 08:26:11', '2018-11-02 07:26:11', 'a:8:{s:4:"type";s:11:"date_picker";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:14:"display_format";s:5:"d/m/Y";s:13:"return_format";s:5:"d/m/Y";s:9:"first_day";i:1;}', 'Date', 'date', 'publish', 'closed', 'closed', '', 'field_5bdbfbeb9a97a', '', '', '2018-11-02 08:26:11', '2018-11-02 07:26:11', '', 44, 'http://coingeek.me/?post_type=acf-field&p=45', 0, 'acf-field', '', 0),
(46, 1, '2018-11-02 08:26:11', '2018-11-02 07:26:11', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Place', 'place', 'publish', 'closed', 'closed', '', 'field_5bdbfc0b9a97b', '', '', '2018-11-02 08:26:11', '2018-11-02 07:26:11', '', 44, 'http://coingeek.me/?post_type=acf-field&p=46', 1, 'acf-field', '', 0),
(47, 1, '2018-11-02 10:31:03', '2018-11-02 09:31:03', '', 'Conferences', '', 'publish', 'closed', 'closed', '', 'conferences', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=47', 10, 'nav_menu_item', '', 0),
(48, 1, '2018-11-02 10:31:03', '2018-11-02 09:31:03', '', 'Hardware', '', 'publish', 'closed', 'closed', '', 'hardware', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=48', 11, 'nav_menu_item', '', 0),
(49, 1, '2018-11-02 10:31:03', '2018-11-02 09:31:03', '', 'Investments', '', 'publish', 'closed', 'closed', '', 'investments', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=49', 12, 'nav_menu_item', '', 0),
(50, 1, '2018-11-02 10:31:04', '2018-11-02 09:31:04', '', 'Mining', '', 'publish', 'closed', 'closed', '', 'mining', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=50', 13, 'nav_menu_item', '', 0),
(51, 1, '2018-11-02 10:31:04', '2018-11-02 09:31:04', '', 'Business Solutions', '', 'publish', 'closed', 'closed', '', 'business-solutions', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=51', 14, 'nav_menu_item', '', 0),
(52, 1, '2018-11-05 03:38:44', '2018-11-05 02:38:44', 'a:8:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"post_template";s:8:"operator";s:2:"==";s:5:"value";s:14:"pages/home.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";s:5:"local";s:3:"php";}', 'Home page settings', 'home-page-settings', 'publish', 'closed', 'closed', '', 'group_5bdfac991097c', '', '', '2018-11-08 05:57:04', '2018-11-08 04:57:04', '', 0, 'http://coingeek.me/?post_type=acf-field-group&#038;p=52', 0, 'acf-field-group', '', 0),
(53, 1, '2018-11-05 03:38:44', '2018-11-05 02:38:44', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Slider', '', 'publish', 'closed', 'closed', '', 'field_5bdfacc87e99a', '', '', '2018-11-08 05:57:10', '2018-11-08 04:57:10', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=53', 0, 'acf-field', '', 0),
(54, 1, '2018-11-05 03:38:44', '2018-11-05 02:38:44', 'a:11:{s:4:"type";s:11:"post_object";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"post_type";a:1:{i:0;s:4:"news";}s:8:"taxonomy";s:0:"";s:10:"allow_null";i:0;s:8:"multiple";i:1;s:13:"return_format";s:6:"object";s:2:"ui";i:1;}', 'Slider Items', 'slider_items', 'publish', 'closed', 'closed', '', 'field_5bdfacdc7e99b', '', '', '2018-11-08 05:57:21', '2018-11-08 04:57:21', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=54', 1, 'acf-field', '', 0),
(55, 1, '2018-11-05 03:41:41', '2018-11-05 02:41:41', 'a:12:{s:4:"type";s:8:"checkbox";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:1:{i:1;s:3:"Oui";}s:12:"allow_custom";i:0;s:13:"default_value";a:1:{i:0;i:1;}s:6:"layout";s:8:"vertical";s:6:"toggle";i:0;s:13:"return_format";s:5:"value";s:11:"save_custom";i:0;}', 'Active', 'slider_active', 'publish', 'closed', 'closed', '', 'field_5bdfad6f09597', '', '', '2018-11-08 05:57:34', '2018-11-08 04:57:34', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=55', 2, 'acf-field', '', 0),
(56, 1, '2018-11-05 03:42:29', '2018-11-05 02:42:29', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-05 03:42:29', '2018-11-05 02:42:29', '', 10, 'http://coingeek.me/2018/11/05/10-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2018-11-05 03:51:28', '2018-11-05 02:51:28', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Latest News', '', 'publish', 'closed', 'closed', '', 'field_5bdfaf7495a2a', '', '', '2018-11-08 05:57:40', '2018-11-08 04:57:40', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=57', 3, 'acf-field', '', 0),
(58, 1, '2018-11-05 03:51:28', '2018-11-05 02:51:28', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:2:"50";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image sidebar', 'image_sidebar', 'publish', 'closed', 'closed', '', 'field_5bdfafcc95a2b', '', '', '2018-11-08 05:57:46', '2018-11-08 04:57:46', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=58', 4, 'acf-field', '', 0),
(59, 1, '2018-11-05 03:51:28', '2018-11-05 02:51:28', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:2:"50";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Image sidebar link', 'image_sidebar_link', 'publish', 'closed', 'closed', '', 'field_5bdfaffb95a2c', '', '', '2018-11-08 05:57:52', '2018-11-08 04:57:52', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=59', 5, 'acf-field', '', 0),
(62, 1, '2018-11-05 04:39:44', '2018-11-05 03:39:44', '', 'register', '', 'inherit', 'open', 'closed', '', 'register', '', '', '2018-11-05 04:39:44', '2018-11-05 03:39:44', '', 10, 'http://coingeek.me/wp-content/uploads/2018/11/register.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2018-11-05 04:40:18', '2018-11-05 03:40:18', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-05 04:40:18', '2018-11-05 03:40:18', '', 10, 'http://coingeek.me/2018/11/05/10-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2018-11-05 05:02:26', '2018-11-05 04:02:26', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Coingeek Week', '', 'publish', 'closed', 'closed', '', 'field_5bdfc034457c7', '', '', '2018-11-08 05:58:19', '2018-11-08 04:58:19', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=64', 9, 'acf-field', '', 0),
(65, 1, '2018-11-05 05:02:26', '2018-11-05 04:02:26', 'a:13:{s:4:"type";s:8:"taxonomy";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:8:"taxonomy";s:12:"category_new";s:10:"field_type";s:6:"select";s:10:"allow_null";i:0;s:8:"add_term";i:1;s:10:"save_terms";i:0;s:10:"load_terms";i:0;s:13:"return_format";s:2:"id";s:8:"multiple";i:0;}', 'Category post', 'category_post', 'publish', 'closed', 'closed', '', 'field_5bdfc077457c8', '', '', '2018-11-08 05:58:28', '2018-11-08 04:58:28', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=65', 10, 'acf-field', '', 0),
(66, 1, '2018-11-05 05:02:26', '2018-11-05 04:02:26', 'a:12:{s:4:"type";s:8:"checkbox";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:1:{i:1;s:4:"True";}s:12:"allow_custom";i:0;s:13:"default_value";a:1:{i:0;i:1;}s:6:"layout";s:8:"vertical";s:6:"toggle";i:0;s:13:"return_format";s:5:"value";s:11:"save_custom";i:0;}', 'Active', 'active_coingeek_week', 'publish', 'closed', 'closed', '', 'field_5bdfc0aa457c9', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://coingeek.me/?post_type=acf-field&#038;p=66', 11, 'acf-field', '', 0),
(67, 1, '2018-11-05 05:09:11', '2018-11-05 04:09:11', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-05 05:09:11', '2018-11-05 04:09:11', '', 10, 'http://coingeek.me/2018/11/05/10-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'News', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=68', 2, 'nav_menu_item', '', 0),
(69, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'All news', '', 'publish', 'closed', 'closed', '', 'all-news', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=69', 3, 'nav_menu_item', '', 0),
(70, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'Business', '', 'publish', 'closed', 'closed', '', 'business', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=70', 4, 'nav_menu_item', '', 0),
(71, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'Editorial', '', 'publish', 'closed', 'closed', '', 'editorial', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=71', 5, 'nav_menu_item', '', 0),
(72, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'Technology', '', 'publish', 'closed', 'closed', '', 'technology', '', '', '2018-11-05 05:50:00', '2018-11-05 04:50:00', '', 0, 'http://coingeek.me/?p=72', 6, 'nav_menu_item', '', 0),
(73, 1, '2018-11-05 05:49:34', '2018-11-05 04:49:34', '', 'Interviews', '', 'publish', 'closed', 'closed', '', 'interviews', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=73', 7, 'nav_menu_item', '', 0),
(74, 1, '2018-11-05 05:49:35', '2018-11-05 04:49:35', '', 'Videos', '', 'publish', 'closed', 'closed', '', 'videos', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=74', 8, 'nav_menu_item', '', 0),
(75, 1, '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 'Events', '', 'publish', 'closed', 'closed', '', 'events', '', '', '2018-11-05 05:50:01', '2018-11-05 04:50:01', '', 0, 'http://coingeek.me/?p=75', 9, 'nav_menu_item', '', 0),
(76, 1, '2018-11-05 07:34:25', '2018-11-05 06:34:25', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"user_form";s:8:"operator";s:2:"==";s:5:"value";s:3:"all";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'User Settings', 'user-settings', 'publish', 'closed', 'closed', '', 'group_5bdfe3fa7a64a', '', '', '2018-11-05 07:34:26', '2018-11-05 06:34:26', '', 0, 'http://coingeek.me/?post_type=acf-field-group&#038;p=76', 0, 'acf-field-group', '', 0),
(77, 1, '2018-11-05 07:34:25', '2018-11-05 06:34:25', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Socials', 'socials', 'publish', 'closed', 'closed', '', 'field_5bdfe4253b156', '', '', '2018-11-05 07:34:25', '2018-11-05 06:34:25', '', 76, 'http://coingeek.me/?post_type=acf-field&p=77', 0, 'acf-field', '', 0),
(78, 1, '2018-11-05 07:34:25', '2018-11-05 06:34:25', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Facebook Link', 'facebook_link', 'publish', 'closed', 'closed', '', 'field_5bdfe43d3b157', '', '', '2018-11-05 07:34:25', '2018-11-05 06:34:25', '', 76, 'http://coingeek.me/?post_type=acf-field&p=78', 1, 'acf-field', '', 0),
(79, 1, '2018-11-05 07:34:26', '2018-11-05 06:34:26', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Facebook Icon', 'facebook_icon', 'publish', 'closed', 'closed', '', 'field_5bdfe4573b158', '', '', '2018-11-05 07:34:26', '2018-11-05 06:34:26', '', 76, 'http://coingeek.me/?post_type=acf-field&p=79', 2, 'acf-field', '', 0),
(80, 1, '2018-11-05 09:38:25', '2018-11-05 08:38:25', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Website Link', 'website_link', 'publish', 'closed', 'closed', '', 'field_5be00178f6df5', '', '', '2018-11-05 09:38:25', '2018-11-05 08:38:25', '', 44, 'http://coingeek.me/?post_type=acf-field&p=80', 2, 'acf-field', '', 0),
(81, 1, '2018-11-05 09:38:51', '2018-11-05 08:38:51', '', 'image-programers', '', 'inherit', 'open', 'closed', '', 'image-programers', '', '', '2018-11-05 09:38:51', '2018-11-05 08:38:51', '', 43, 'http://coingeek.me/wp-content/uploads/2018/11/image-programers.jpg', 0, 'attachment', 'image/jpeg', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(82, 1, '2018-11-05 11:18:58', '2018-11-05 10:18:58', '<h2>What is Lorem Ipsum?</h2>\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n<h2>Where does it come from?</h2>\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\n\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\n<img class="alignnone wp-image-83 size-full" src="http://coingeek.me/wp-content/uploads/2018/11/Screenshot_1.png" alt="" width="1860" height="897" />\n<p>When Brazilian-born Henrique Dubugras and Pedro Franceschi met at 16 years old, they bonded over a love of coding and mutual frustrations with their strict mothers, who didn’t understand their Mark Zuckerberg-esque ambitions.</p>\n                <p>To be fair, their moms’ fear of their hacking habits only escalated after their pre-teen sons received legal notices of patent infringements in the mail. A legal threat from Apple, which Franceschi received after discovering the first jailbreak to the iPhone, is enough to warrant a grounding, at the very least.</p>\n                <p>Their parents implored them to quit the hacking and stop messing around online.</p>\n\n                <p>They didn’t listen.</p>\n\n                <p>Today, the now 22-year-olds are announcing a $125 million Series C for their second successful payments business, called Brex, at a $1.1 billion valuation. Greenoaks Capital, DST Global and IVP led the round, which brings their total raised to date to about $200 million.\n                </p>\n\n                <p>Today, the now 22-year-olds are announcing a $125 million Series C for their second successful payments business, called Brex, at a $1.1 billion valuation. Greenoaks Capital, DST Global and IVP led the round, which brings their total raised to date to about $200 million.\n                </p>\n                <p>San Francisco-based Brex provides startup founders access to corporate credit cards without a personal guarantee or deposit. It’s also supported by the likes of PayPal founders Peter Thiel and Max Levchin, the former chief executive officer of Visa Carl Pascarella and a handful of leading venture capital firms.\n                </p>', 'Test 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'inherit', 'closed', 'closed', '', '16-autosave-v1', '', '', '2018-11-05 11:18:58', '2018-11-05 10:18:58', '', 16, 'http://coingeek.me/2018/11/05/16-autosave-v1/', 0, 'revision', '', 0),
(83, 1, '2018-11-05 11:18:16', '2018-11-05 10:18:16', '', 'Screenshot_1', '', 'inherit', 'open', 'closed', '', 'screenshot_1', '', '', '2018-11-05 11:18:16', '2018-11-05 10:18:16', '', 16, 'http://coingeek.me/wp-content/uploads/2018/11/Screenshot_1.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2018-11-06 08:59:29', '2018-11-06 07:59:29', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"news";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'New Settings', 'new-settings', 'publish', 'closed', 'closed', '', 'group_5be14995c4f66', '', '', '2018-11-06 08:59:29', '2018-11-06 07:59:29', '', 0, 'http://coingeek.me/?post_type=acf-field-group&#038;p=84', 0, 'acf-field-group', '', 0),
(85, 1, '2018-11-06 08:59:29', '2018-11-06 07:59:29', 'a:12:{s:4:"type";s:5:"radio";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:2:{i:0;s:16:"Default Template";i:1;s:16:"Sidebar Template";}s:10:"allow_null";i:0;s:12:"other_choice";i:0;s:13:"default_value";i:0;s:6:"layout";s:8:"vertical";s:13:"return_format";s:5:"value";s:17:"save_other_choice";i:0;}', 'Template', 'template', 'publish', 'closed', 'closed', '', 'field_5be149a2c6927', '', '', '2018-11-06 08:59:29', '2018-11-06 07:59:29', '', 84, 'http://coingeek.me/?post_type=acf-field&p=85', 0, 'acf-field', '', 0),
(86, 1, '2018-11-07 05:31:03', '2018-11-07 04:31:03', '', 'DSC100312324', '', 'inherit', 'open', 'closed', '', 'dsc100312324', '', '', '2018-11-07 05:31:03', '2018-11-07 04:31:03', '', 17, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/DSC100312324.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2018-11-07 05:31:44', '2018-11-07 04:31:44', '', 'Wallpaper-v4-1920x1080', '', 'inherit', 'open', 'closed', '', 'wallpaper-v4-1920x1080', '', '', '2018-11-07 05:31:44', '2018-11-07 04:31:44', '', 16, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/Wallpaper-v4-1920x1080.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2018-11-07 05:32:11', '2018-11-07 04:32:11', '', 'Multi-Color-Wallpaper-17-1920x1080', '', 'inherit', 'open', 'closed', '', 'multi-color-wallpaper-17-1920x1080', '', '', '2018-11-07 05:32:11', '2018-11-07 04:32:11', '', 14, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/Multi-Color-Wallpaper-17-1920x1080.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2018-11-07 05:47:12', '2018-11-07 04:47:12', '', 'Darksiders-Wrath-of-War_1920x1080', '', 'inherit', 'open', 'closed', '', 'darksiders-wrath-of-war_1920x1080', '', '', '2018-11-07 05:47:12', '2018-11-07 04:47:12', '', 15, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/Darksiders-Wrath-of-War_1920x1080.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2018-11-07 06:53:42', '2018-11-07 05:53:42', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Business 6', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-6', '', '', '2018-11-08 08:28:27', '2018-11-08 07:28:27', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=92', 0, 'news', '', 0),
(93, 1, '2018-11-07 06:54:27', '2018-11-07 05:54:27', '<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 'Business 7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and..', 'publish', 'open', 'closed', '', 'test-7', '', '', '2018-11-08 08:28:45', '2018-11-08 07:28:45', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=93', 0, 'news', '', 0),
(94, 1, '2018-11-07 08:21:36', '2018-11-07 07:21:36', '', 'sidebar', '', 'inherit', 'open', 'closed', '', 'sidebar', '', '', '2018-11-07 08:21:36', '2018-11-07 07:21:36', '', 10, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/sidebar.jpg', 0, 'attachment', 'image/jpeg', 0),
(95, 1, '2018-11-07 08:21:40', '2018-11-07 07:21:40', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-07 08:21:40', '2018-11-07 07:21:40', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2018-11-07 08:28:56', '2018-11-07 07:28:56', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Banner Image', 'business_banner_image', 'publish', 'closed', 'closed', '', 'field_5be2915e7c9a6', '', '', '2018-11-08 05:58:08', '2018-11-08 04:58:08', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&#038;p=96', 7, 'acf-field', '', 0),
(97, 1, '2018-11-07 08:28:56', '2018-11-07 07:28:56', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Banner Link', 'business_banner_link', 'publish', 'closed', 'closed', '', 'field_5be291797c9a7', '', '', '2018-11-08 05:58:14', '2018-11-08 04:58:14', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&#038;p=97', 8, 'acf-field', '', 0),
(98, 1, '2018-11-07 08:28:56', '2018-11-07 07:28:56', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Event Section', '', 'publish', 'closed', 'closed', '', 'field_5be28f151cabb', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&#038;p=98', 12, 'acf-field', '', 0),
(99, 1, '2018-11-07 08:28:56', '2018-11-07 07:28:56', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Banner Image', 'coingeek_banner_image', 'publish', 'closed', 'closed', '', 'field_5be28a5d13d5d', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&#038;p=99', 13, 'acf-field', '', 0),
(100, 1, '2018-11-07 08:28:56', '2018-11-07 07:28:56', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Banner Link', 'coingeek_banner_link', 'publish', 'closed', 'closed', '', 'field_5be28a7f13d5e', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&#038;p=100', 14, 'acf-field', '', 0),
(101, 1, '2018-11-07 08:29:26', '2018-11-07 07:29:26', '', 'banner_business', '', 'inherit', 'open', 'closed', '', 'banner_business', '', '', '2018-11-07 08:29:26', '2018-11-07 07:29:26', '', 10, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/banner_business.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2018-11-07 08:29:27', '2018-11-07 07:29:27', '', 'banner_coin', '', 'inherit', 'open', 'closed', '', 'banner_coin', '', '', '2018-11-07 08:29:27', '2018-11-07 07:29:27', '', 10, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/banner_coin.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2018-11-07 08:29:44', '2018-11-07 07:29:44', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-07 08:29:44', '2018-11-07 07:29:44', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2018-11-07 08:30:04', '2018-11-07 07:30:04', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-07 08:30:04', '2018-11-07 07:30:04', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2018-11-07 11:04:45', '2018-11-07 10:04:45', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";i:0;s:3:"max";i:0;s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Footer Image', 'footer_image', 'publish', 'closed', 'closed', '', 'field_5be2a3e9f2ba1', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 6, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=105', 5, 'acf-field', '', 0),
(106, 1, '2018-11-07 11:04:45', '2018-11-07 10:04:45', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Image', 'image', 'publish', 'closed', 'closed', '', 'field_5be2a410f2ba2', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 105, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=106', 0, 'acf-field', '', 0),
(107, 1, '2018-11-07 11:04:45', '2018-11-07 10:04:45', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_5be2a41cf2ba3', '', '', '2018-11-07 11:04:45', '2018-11-07 10:04:45', '', 105, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=107', 1, 'acf-field', '', 0),
(108, 1, '2018-11-07 11:07:14', '2018-11-07 10:07:14', '', 'AyreGroup-01', '', 'inherit', 'open', 'closed', '', 'ayregroup-01', '', '', '2018-11-07 11:07:14', '2018-11-07 10:07:14', '', 0, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/AyreGroup-01.svg', 0, 'attachment', 'image/svg+xml', 0),
(109, 1, '2018-11-07 11:07:15', '2018-11-07 10:07:15', '', 'AyreMedia-01', '', 'inherit', 'open', 'closed', '', 'ayremedia-01', '', '', '2018-11-07 11:07:15', '2018-11-07 10:07:15', '', 0, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/AyreMedia-01.svg', 0, 'attachment', 'image/svg+xml', 0),
(110, 1, '2018-11-07 11:07:16', '2018-11-07 10:07:16', '', 'CalvinAyre-01-2', '', 'inherit', 'open', 'closed', '', 'calvinayre-01-2', '', '', '2018-11-07 11:07:16', '2018-11-07 10:07:16', '', 0, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/CalvinAyre-01-2.svg', 0, 'attachment', 'image/svg+xml', 0),
(111, 1, '2018-11-07 11:07:17', '2018-11-07 10:07:17', '', 'CalvinAyreFoundation-01', '', 'inherit', 'open', 'closed', '', 'calvinayrefoundation-01', '', '', '2018-11-07 11:07:17', '2018-11-07 10:07:17', '', 0, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/CalvinAyreFoundation-01.svg', 0, 'attachment', 'image/svg+xml', 0),
(112, 1, '2018-11-08 05:56:34', '2018-11-08 04:56:34', 'a:8:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:18:"acf-options-blocks";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";s:5:"local";s:3:"php";}', 'Block Settings', 'block-settings', 'publish', 'closed', 'closed', '', 'group_5be3a2bcd912e', '', '', '2018-11-08 05:56:34', '2018-11-08 04:56:34', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field-group&p=112', 0, 'acf-field-group', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(113, 1, '2018-11-08 05:56:40', '2018-11-08 04:56:40', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Footer Home', '', 'publish', 'closed', 'closed', '', 'field_5be3a2bfdffbd', '', '', '2018-11-08 05:56:40', '2018-11-08 04:56:40', '', 112, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=113', 0, 'acf-field', '', 0),
(114, 1, '2018-11-08 05:56:50', '2018-11-08 04:56:50', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Logo', 'logo_home_footer', 'publish', 'closed', 'closed', '', 'field_5be3a2e8dffbe', '', '', '2018-11-08 05:56:50', '2018-11-08 04:56:50', '', 112, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=114', 1, 'acf-field', '', 0),
(115, 1, '2018-11-08 05:56:59', '2018-11-08 04:56:59', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_5be3a2f5dffbf', '', '', '2018-11-08 05:56:59', '2018-11-08 04:56:59', '', 112, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=115', 2, 'acf-field', '', 0),
(116, 1, '2018-11-08 05:57:56', '2018-11-08 04:57:56', 'a:13:{s:4:"type";s:8:"taxonomy";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:8:"taxonomy";s:12:"category_new";s:10:"field_type";s:6:"select";s:10:"allow_null";i:0;s:8:"add_term";i:1;s:10:"save_terms";i:0;s:10:"load_terms";i:0;s:13:"return_format";s:2:"id";s:8:"multiple";i:0;}', 'Business Articles', 'business_articles', 'publish', 'closed', 'closed', '', 'field_5be3b68b7e58e', '', '', '2018-11-08 05:57:56', '2018-11-08 04:57:56', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=116', 6, 'acf-field', '', 0),
(117, 1, '2018-11-08 05:58:33', '2018-11-08 04:58:33', 'a:13:{s:4:"type";s:8:"taxonomy";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:8:"taxonomy";s:12:"category_new";s:10:"field_type";s:6:"select";s:10:"allow_null";i:0;s:8:"add_term";i:1;s:10:"save_terms";i:0;s:10:"load_terms";i:0;s:13:"return_format";s:2:"id";s:8:"multiple";i:0;}', 'Technology Articles', 'technology_articles', 'publish', 'closed', 'closed', '', 'field_5be3b9da8f03b', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=117', 15, 'acf-field', '', 0),
(118, 1, '2018-11-08 05:58:33', '2018-11-08 04:58:33', 'a:13:{s:4:"type";s:8:"taxonomy";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:8:"taxonomy";s:12:"category_new";s:10:"field_type";s:6:"select";s:10:"allow_null";i:0;s:8:"add_term";i:1;s:10:"save_terms";i:0;s:10:"load_terms";i:0;s:13:"return_format";s:2:"id";s:8:"multiple";i:0;}', 'Video Articles', 'video_articles', 'publish', 'closed', 'closed', '', 'field_5be3b6df7e58f', '', '', '2018-11-08 05:58:33', '2018-11-08 04:58:33', '', 52, 'http://wordpress.coingeek.staging.fides.io/?post_type=acf-field&p=118', 16, 'acf-field', '', 0),
(119, 1, '2018-11-08 06:00:46', '2018-11-08 05:00:46', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-08 06:00:46', '2018-11-08 05:00:46', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2018-11-08 06:06:51', '2018-11-08 05:06:51', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 1', '', 'publish', 'open', 'closed', '', 'technology-1', '', '', '2018-11-08 06:06:51', '2018-11-08 05:06:51', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=120', 0, 'news', '', 0),
(121, 1, '2018-11-08 06:17:40', '2018-11-08 05:17:40', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 2', '', 'publish', 'open', 'closed', '', 'technology-2', '', '', '2018-11-08 06:17:40', '2018-11-08 05:17:40', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=121', 0, 'news', '', 0),
(122, 1, '2018-11-08 06:18:23', '2018-11-08 05:18:23', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 3', '', 'publish', 'open', 'closed', '', 'technology-3', '', '', '2018-11-08 06:18:23', '2018-11-08 05:18:23', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=122', 0, 'news', '', 0),
(123, 1, '2018-11-08 06:20:48', '2018-11-08 05:20:48', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 1', '', 'publish', 'open', 'closed', '', 'video-1', '', '', '2018-11-08 06:21:49', '2018-11-08 05:21:49', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=123', 0, 'news', '', 0),
(124, 1, '2018-11-08 08:24:45', '2018-11-08 07:24:45', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 2', '', 'publish', 'open', 'closed', '', 'video-2', '', '', '2018-11-08 08:24:45', '2018-11-08 07:24:45', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=124', 0, 'news', '', 0),
(125, 1, '2018-11-08 08:09:55', '2018-11-08 07:09:55', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 4', '', 'publish', 'open', 'closed', '', 'technology-4', '', '', '2018-11-08 08:09:55', '2018-11-08 07:09:55', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=125', 0, 'news', '', 0),
(126, 1, '2018-11-08 08:10:50', '2018-11-08 07:10:50', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 5', '', 'publish', 'open', 'closed', '', 'technology-5', '', '', '2018-11-08 08:10:50', '2018-11-08 07:10:50', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=126', 0, 'news', '', 0),
(127, 1, '2018-11-08 08:23:33', '2018-11-08 07:23:33', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 6', '', 'publish', 'open', 'closed', '', 'technology-6', '', '', '2018-11-08 08:23:33', '2018-11-08 07:23:33', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=127', 0, 'news', '', 0),
(128, 1, '2018-11-08 08:23:49', '2018-11-08 07:23:49', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Technology 7', '', 'publish', 'open', 'closed', '', 'technology-7', '', '', '2018-11-08 08:23:49', '2018-11-08 07:23:49', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=128', 0, 'news', '', 0),
(129, 1, '2018-11-08 08:25:30', '2018-11-08 07:25:30', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 3', '', 'publish', 'open', 'closed', '', 'video-3', '', '', '2018-11-08 08:25:30', '2018-11-08 07:25:30', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=129', 0, 'news', '', 0),
(130, 1, '2018-11-08 08:26:26', '2018-11-08 07:26:26', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 6', '', 'publish', 'open', 'closed', '', 'video-6', '', '', '2018-11-08 08:26:26', '2018-11-08 07:26:26', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=130', 0, 'news', '', 0),
(131, 1, '2018-11-08 08:26:15', '2018-11-08 07:26:15', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 5', '', 'publish', 'open', 'closed', '', 'video-5', '', '', '2018-11-08 08:26:15', '2018-11-08 07:26:15', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=131', 0, 'news', '', 0),
(132, 1, '2018-11-08 08:25:47', '2018-11-08 07:25:47', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Video 4', '', 'publish', 'open', 'closed', '', 'video-4', '', '', '2018-11-08 08:25:47', '2018-11-08 07:25:47', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=132', 0, 'news', '', 0),
(133, 1, '2018-11-08 08:29:38', '2018-11-08 07:29:38', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-08 08:29:38', '2018-11-08 07:29:38', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2018-11-08 08:31:15', '2018-11-08 07:31:15', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 1', '', 'publish', 'open', 'closed', '', 'event-1', '', '', '2018-11-08 08:31:15', '2018-11-08 07:31:15', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=134', 0, 'news', '', 0),
(135, 1, '2018-11-08 08:32:50', '2018-11-08 07:32:50', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 6', '', 'publish', 'open', 'closed', '', 'event-6', '', '', '2018-11-08 08:32:50', '2018-11-08 07:32:50', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=135', 0, 'news', '', 0),
(136, 1, '2018-11-08 08:32:23', '2018-11-08 07:32:23', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 5', '', 'publish', 'open', 'closed', '', 'event-5', '', '', '2018-11-08 08:32:23', '2018-11-08 07:32:23', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=136', 0, 'news', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(137, 1, '2018-11-08 08:32:02', '2018-11-08 07:32:02', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 4', '', 'publish', 'open', 'closed', '', 'event-4', '', '', '2018-11-08 08:32:02', '2018-11-08 07:32:02', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=137', 0, 'news', '', 0),
(138, 1, '2018-11-08 08:31:47', '2018-11-08 07:31:47', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 3', '', 'publish', 'open', 'closed', '', 'event-3', '', '', '2018-11-08 08:31:47', '2018-11-08 07:31:47', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=138', 0, 'news', '', 0),
(139, 1, '2018-11-08 08:31:32', '2018-11-08 07:31:32', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n</div>\r\n&nbsp;\r\n<div>\r\n<h2>Where does it come from?</h2>\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\n</div>', 'Event 2', '', 'publish', 'open', 'closed', '', 'event-2', '', '', '2018-11-08 08:31:32', '2018-11-08 07:31:32', '', 0, 'http://wordpress.coingeek.staging.fides.io/?post_type=news&#038;p=139', 0, 'news', '', 0),
(140, 1, '2018-11-08 08:33:29', '2018-11-08 07:33:29', '', 'Home', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-11-08 08:33:29', '2018-11-08 07:33:29', '', 10, 'http://wordpress.coingeek.staging.fides.io/10-revision-v1/', 0, 'revision', '', 0),
(141, 1, '2018-11-08 11:21:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-11-08 11:21:14', '0000-00-00 00:00:00', '', 0, 'http://wordpress.coingeek.staging.fides.io/?p=141', 0, 'post', '', 0),
(142, 1, '2018-11-08 11:21:33', '2018-11-08 10:21:33', '', 'lg', '', 'inherit', 'open', 'closed', '', 'lg', '', '', '2018-11-08 11:21:33', '2018-11-08 10:21:33', '', 0, 'http://wordpress.coingeek.staging.fides.io/wp-content/uploads/2018/11/lg.jpg', 0, 'attachment', 'image/jpeg', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_sib_model_forms`
#

DROP TABLE IF EXISTS `wp_sib_model_forms`;


#
# Table structure of table `wp_sib_model_forms`
#

CREATE TABLE `wp_sib_model_forms` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `css` longtext,
  `dependTheme` int(1) NOT NULL DEFAULT '1',
  `listID` longtext,
  `templateID` int(20) NOT NULL DEFAULT '-1',
  `confirmID` int(20) NOT NULL DEFAULT '-1',
  `isDopt` int(1) NOT NULL DEFAULT '0',
  `isOpt` int(1) NOT NULL DEFAULT '0',
  `redirectInEmail` varchar(255) DEFAULT NULL,
  `redirectInForm` varchar(255) DEFAULT NULL,
  `successMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `errorMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `existMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiredMsg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `isDefault` int(1) NOT NULL DEFAULT '0',
  `gCaptcha` int(1) NOT NULL DEFAULT '0',
  `gCaptcha_secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gCaptcha_site` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `termAccept` int(1) NOT NULL DEFAULT '0',
  `termsURL` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


#
# Data contents of table `wp_sib_model_forms`
#
INSERT INTO `wp_sib_model_forms` ( `id`, `title`, `html`, `css`, `dependTheme`, `listID`, `templateID`, `confirmID`, `isDopt`, `isOpt`, `redirectInEmail`, `redirectInForm`, `successMsg`, `errorMsg`, `existMsg`, `invalidMsg`, `requiredMsg`, `attributes`, `date`, `isDefault`, `gCaptcha`, `gCaptcha_secret`, `gCaptcha_site`, `termAccept`, `termsURL`) VALUES
(1, 'Default Form', '<p class="sib-email-area">\n    <label class="sib-email-area">Email Address*</label>\n    <input type="email" class="sib-email-area" name="email" required="required">\n</p>\n<p class="sib-NAME-area">\n    <label class="sib-NAME-area">Name</label>\n    <input type="text" class="sib-NAME-area" name="NAME" >\n</p>\n<p>\n    <input type="submit" class="sib-default-btn" value="Subscribe">\n</p>', '[form] {\n    padding: 5px;\n    -moz-box-sizing:border-box;\n    -webkit-box-sizing: border-box;\n    box-sizing: border-box;\n}\n[form] input[type=text],[form] input[type=email], [form] select {\n    width: 100%;\n    border: 1px solid #bbb;\n    height: auto;\n    margin: 5px 0 0 0;\n}\n[form] .sib-default-btn {\n    margin: 5px 0;\n    padding: 6px 12px;\n    color:#fff;\n    background-color: #333;\n    border-color: #2E2E2E;\n    font-size: 14px;\n    font-weight:400;\n    line-height: 1.4285;\n    text-align: center;\n    cursor: pointer;\n    vertical-align: middle;\n    -webkit-user-select:none;\n    -moz-user-select:none;\n    -ms-user-select:none;\n    user-select:none;\n    white-space: normal;\n    border:1px solid transparent;\n    border-radius: 3px;\n}\n[form] .sib-default-btn:hover {\n    background-color: #444;\n}\n[form] p{\n    margin: 10px 0 0 0;\n}', 1, 'a:1:{i:0;s:0:"";}', -1, -1, 0, 0, NULL, NULL, 'Thank you, you have successfully registered !', 'Something wrong occured', 'You have already registered', 'Your email address is invalid', 'Please fill out this field', 'email,NAME', '2018-11-01', 1, 0, NULL, NULL, 0, NULL),
(2, 'Footer Form', '<input type="email" class="sib-email-area" name="email" required="required" placeholder="Enter your email address">\r\n                        <input type="submit" value="subscribe" class="cg__news">', '[form] {\r\n    padding: 5px;\r\n    -moz-box-sizing:border-box;\r\n    -webkit-box-sizing: border-box;\r\n    box-sizing: border-box;\r\n}\r\n[form] input[type=text],[form] input[type=email], [form] select {\r\n    width: 100%;\r\n    border: 1px solid #bbb;\r\n    height: auto;\r\n    margin: 5px 0 0 0;\r\n}\r\n[form] .sib-default-btn {\r\n    margin: 5px 0;\r\n    padding: 6px 12px;\r\n    color:#fff;\r\n    background-color: #333;\r\n    border-color: #2E2E2E;\r\n    font-size: 14px;\r\n    font-weight:400;\r\n    line-height: 1.4285;\r\n    text-align: center;\r\n    cursor: pointer;\r\n    vertical-align: middle;\r\n    -webkit-user-select:none;\r\n    -moz-user-select:none;\r\n    -ms-user-select:none;\r\n    user-select:none;\r\n    white-space: normal;\r\n    border:1px solid transparent;\r\n    border-radius: 3px;\r\n}\r\n[form] .sib-default-btn:hover {\r\n    background-color: #444;\r\n}\r\n[form] p{\r\n    margin: 10px 0 0 0;\r\n}', 1, '', -1, -1, 0, 0, '', '', 'Thank you, you have successfully registered !', 'Something wrong occured', 'You have already registered', 'Your email address is invalid', 'Please fill out this field', 'email', '2018-11-01', 1, 0, '', '', 0, '') ;

#
# End of data contents of table `wp_sib_model_forms`
# --------------------------------------------------------



#
# Delete any existing table `wp_sib_model_users`
#

DROP TABLE IF EXISTS `wp_sib_model_users`;


#
# Table structure of table `wp_sib_model_users`
#

CREATE TABLE `wp_sib_model_users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `listIDs` longtext,
  `redirectUrl` varchar(255) DEFAULT NULL,
  `info` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `frmid` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


#
# Data contents of table `wp_sib_model_users`
#

#
# End of data contents of table `wp_sib_model_users`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(14, 1, 0),
(14, 5, 0),
(14, 6, 0),
(14, 7, 0),
(14, 8, 0),
(14, 10, 0),
(14, 11, 0),
(15, 1, 0),
(15, 5, 0),
(15, 6, 0),
(15, 7, 0),
(15, 8, 0),
(15, 10, 0),
(15, 11, 0),
(16, 1, 0),
(16, 5, 0),
(16, 6, 0),
(16, 7, 0),
(16, 8, 0),
(16, 10, 0),
(16, 11, 0),
(17, 1, 0),
(17, 5, 0),
(17, 6, 0),
(17, 7, 0),
(17, 8, 0),
(17, 10, 0),
(17, 11, 0),
(24, 2, 0),
(31, 3, 0),
(32, 3, 0),
(33, 3, 0),
(34, 3, 0),
(35, 4, 0),
(36, 4, 0),
(37, 4, 0),
(38, 4, 0),
(41, 17, 0),
(42, 18, 0),
(43, 17, 0),
(47, 2, 0),
(48, 2, 0),
(49, 2, 0),
(50, 2, 0),
(51, 2, 0),
(68, 2, 0),
(69, 2, 0),
(70, 2, 0),
(71, 2, 0),
(72, 2, 0),
(73, 2, 0),
(74, 2, 0),
(75, 2, 0),
(92, 11, 0),
(93, 11, 0),
(120, 19, 0),
(121, 19, 0),
(122, 19, 0),
(123, 16, 0),
(124, 16, 0),
(125, 19, 0),
(126, 19, 0),
(127, 19, 0),
(128, 19, 0),
(129, 16, 0),
(130, 16, 0),
(131, 16, 0),
(132, 16, 0),
(134, 13, 0),
(135, 13, 0),
(136, 13, 0),
(137, 13, 0),
(138, 13, 0),
(139, 13, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 5),
(2, 2, 'nav_menu', '', 0, 14),
(3, 3, 'nav_menu', '', 0, 4),
(4, 4, 'nav_menu', '', 0, 4),
(5, 5, 'category', '', 0, 4),
(6, 6, 'category', '', 0, 4),
(7, 7, 'category', '', 0, 4),
(8, 8, 'category', '', 0, 4),
(9, 9, 'category', '', 0, 0),
(10, 10, 'category', '', 0, 4),
(11, 11, 'category_new', '', 0, 6),
(12, 12, 'category_new', '', 0, 0),
(13, 13, 'category_new', '', 0, 6),
(14, 14, 'category_new', '', 0, 0),
(15, 15, 'category_new', '', 0, 0),
(16, 16, 'category_new', '', 0, 6),
(17, 17, 'category_event', '', 0, 2),
(18, 18, 'category_event', '', 0, 1),
(19, 19, 'category_new', '', 0, 7) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Non classé', 'non-classe', 0),
(2, 'header_menu', 'header_menu', 0),
(3, 'footer-menu-left', 'footer-menu-left', 0),
(4, 'footer-menu-right', 'footer-menu-right', 0),
(5, 'Business', 'business', 0),
(6, 'Editorial', 'editorial', 0),
(7, 'Events', 'events', 0),
(8, 'Interviews', 'interviews', 0),
(9, 'Tech', 'tech', 0),
(10, 'Press Releases', 'press-releases', 0),
(11, 'Business', 'business', 0),
(12, 'Editorial', 'editorial', 0),
(13, 'Events', 'events', 0),
(14, 'Interviews', 'interviews', 0),
(15, 'Press Releases', 'press-releases', 0),
(16, 'Video', 'video', 0),
(17, 'test 1', 'test-1', 0),
(18, 'test 2', 'test-2', 0),
(19, 'Technology', 'technology', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,text_widget_custom_html'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:7:{s:64:"fec1961fe2fb2ada6815a5773a42735cde720b09b841134c0e52c0c73d13267b";a:4:{s:10:"expiration";i:1542685877;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541476277;}s:64:"11628c91d3efa0cc9871ecb8fe587c528666946d6430b1a4390b0f0db2d8ed13";a:4:{s:10:"expiration";i:1542769151;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541559551;}s:64:"f2b751531e7c7dd54c46fb873e0b6c61051bbd046c094fd3d150c7d147c46135";a:4:{s:10:"expiration";i:1542769367;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541559767;}s:64:"abbb3fbb54d69a178fe0bd3e19baf3367e0c96717cde53d02c0f17eafe2aa129";a:4:{s:10:"expiration";i:1541732976;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541560176;}s:64:"9f46efa70b49e1453d70ca38d8ba6ad97bbcb826c4dd689721117f8ff003275a";a:4:{s:10:"expiration";i:1542774050;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541564450;}s:64:"091604680eefee81fca26441e9ae7bc7138b4776137e174d9bb1a668e60e2db1";a:4:{s:10:"expiration";i:1541738256;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541565456;}s:64:"7271d6acca1a59a72d496de90b39c9ed86eb12e8dc440dc3f67d405a4062f13e";a:4:{s:10:"expiration";i:1542775344;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";s:5:"login";i:1541565744;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '141'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:11:"27.74.253.0";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce'),
(20, 1, 'wp_user-settings-time', '1541413137'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:24:"add-post-type-actualites";i:1;s:12:"add-post_tag";}'),
(23, 1, 'nav_menu_recently_edited', '2'),
(24, 1, 'show_try_gutenberg_panel', '0'),
(25, 1, 'facebook_link', ''),
(26, 1, '_facebook_link', 'field_5bdfe43d3b157'),
(27, 1, 'facebook_icon', ''),
(28, 1, '_facebook_icon', 'field_5bdfe4573b158') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BE1Jbyf9VnGRAPLze7mIQMZ8jOV8BD.', 'fidesio', 'quyen.le@fidesio.com', '', '2018-11-01 04:47:00', '', 0, 'Admin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

