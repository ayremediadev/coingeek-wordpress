<?php get_header(); ?>
<main id="main-content">
    <div class="wrapper">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">

                <h2><?php the_title(); ?></h2>
                <div class="content">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>

</main>
<?php get_footer(); ?>
