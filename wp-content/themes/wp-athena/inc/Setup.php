<?php
/**
 * Enqueue scripts and styles.
**/
function athena_scripts() {
    // Styles
    wp_enqueue_style( 'main-style', ASSETS_PATH.'css/main.css', array(), null );

    // Scripts
    wp_enqueue_script( 'main-script', ASSETS_PATH.'js/main.js', array('jquery'), null, true );

    wp_localize_script( 'main-script', 'wp_localize',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'homeurl' => get_home_url()
        )
    );
    wp_enqueue_script( 'main-script' );

}
add_action( 'wp_enqueue_scripts', 'athena_scripts' );

/**
 * Register Menu
**/
add_action('init', 'athena_setup');
function athena_setup(){
    register_nav_menus( array(
        'athena_main_menu' => __('Main Menu', DOMAIN)
    ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array( 'search-form' ) );
    add_post_type_support( 'page', 'excerpt' );
    // add_image_size(FULL_SLIDE,1366,555,TRUE);
}
/**
 * Add Image Size
**/
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( IMG_NEW_NORMAL, 380, 188, true );
    add_image_size( IMG_NEW_SIDEBAR, 280, 460, true );
    add_image_size( IMG_SLIDER_HOME, 680, 354, true );
    add_image_size( IMG_NEW_BANNER, 1920, 500, true );
    add_image_size( IMG_LATEST_NEWS, 280, 139, true );
    add_image_size( IMG_BUSINESS_THUMB, 120, 80, true );
    add_image_size( IMG_COINGEEK, 280, 220, true );
    add_image_size( IMG_COINGEEK_LARGE, 280, 460, true );
    add_image_size( IMG_EVENT_DETAIL, 1200, 500, true );
}

