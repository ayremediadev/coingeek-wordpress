<?php
function wpb_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Home Footer', DOMAIN ),
        'id' => 'home-footer',
        'description' => __( 'Home Footer', DOMAIN),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ) );
}

add_action( 'widgets_init', 'wpb_widgets_init' );


//change number post of news post type
function wpsites_query( $query ) {
    if ( ($query->is_tax('category_new')|| $query->is_post_type_archive('news')) && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 2 );
    }
}
add_action( 'pre_get_posts', 'wpsites_query' );

//get news by ajax
add_action( 'wp_ajax_get_news_by_taxonomy', 'get_news_by_taxonomy' );
add_action( 'wp_ajax_nopriv_get_news_by_taxonomy', 'get_news_by_taxonomy' );

function get_news_by_taxonomy(){
    global $wp_query;
    $paged = !empty($_POST['page'])?$_POST['page']:0;
    $tax = !empty($_POST['tax'])?$_POST['tax']:"";
    $cat = !empty($_POST['cat'])?$_POST['cat']:"";
    $count_pages = !empty($_POST['max-page'])?$_POST['max-page']:0;
    if($paged < $count_pages){
        $args = array(
            'post_type' => array(
                'news'
            ),
            'post_status' => array(
                'publish'
            ),
            'order' => 'DESC',
            'orderby' => 'date',
            'tax_query' => $cat ? array(
                array(
                    'taxonomy' => $tax,
                    'field' => 'slug',
                    'terms' => $cat,
                ),
            ) : "",
            'posts_per_page' => 2,
            'paged' => $paged+1
        );
        $query = new WP_Query($args);
        ob_start();
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                ?>
                <div class="col-3">
                    <?php get_template_part('template_parts/new'); ?>
                </div>
                <?php
            }
        }
        $response = ob_get_contents();
        ob_end_clean();
        echo $response;
    }
    wp_die();
}