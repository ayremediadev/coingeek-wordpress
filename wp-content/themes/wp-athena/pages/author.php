<?php
$users = get_users(array('fields' => array( 'ID' )));
/**
 * Template Name: Page Authors
 */
get_header();
?>
    <main id="main-content">

        <div class="title__non-results">
            <div class="wrapper">
                <div class="row">
                    <div class="col-8">
                        <p>Coingeek is your trusted source for all of the latest cryptocurrency news.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="include__top--list--author">
            <div class="wrapper">
                <div class="row">
                    <div class="col-8">
                        <div class="text">
                            <h3>CoinGeek’s Editorial Team: News Writers, Reporters, and Contributors</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis eros vitae lectus viverra hendrerit. Vivamus ac velit id purus efficitur rutrum non ut nulla. In nisl nibh, luctus vel quam eu, fermentum sodales tellus. Morbi nec
                                magna efficitur, mollis dui nec, tempus ante. Aenean a porta arcu. Nulla iaculis arcu laoreet, molestie eros id, blandit sapien.</p>
                        </div>
                        <div class="botton__see--web">
                            <a class="contact" href="#"><span>contact</span></a>
                        </div>
                        <div class="about">
                            <a href="#"> about us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box__list--author">
            <div class="wrapper">
                <div class="row">
                    <?php
                    foreach($users as $user_id){
                        print_r(get_user_meta ( $user_id->ID));
                    }
                    ?>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="box__list--author">
            <div class="wrapper">
                <div class="row">
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                    <div class="col-3">
                        <div class="box__image">
                            <img src="../wp-content/themes/wp-athena/assets/images/image-180.jpg" alt="a">
                            <a href="#"></a>
                        </div>
                        <div class="info">
                            <h4>BECKY LIGGERO</h4>
                            <p>Industry Reporter &amp; Interviewer</p>
                        </div>
                        <div class="soial">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="icon-Facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-Youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a class="see__more" href="#">View profile</a>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--Start Pull HTML here-->
    <!--END  Pull HTML here-->
<?php get_footer(); ?>