<?php get_header(); ?>
<main id="main-content">
    <?php get_template_part('template_parts/event__filter'); ?>
    <div class="wrapper">

        <?php if (have_posts()) : ?>
            <div class="row">
            <?php while (have_posts()) : the_post(); ?>

                <div class="col-3">
                    <?php get_template_part('template_parts/metting__item'); ?>
                </div>

            <?php endwhile; ?>
            </div>
        <?php else : ?>

            <h2>Nothing found</h2>

        <?php endif; ?>
    </div>

</main>

<?php get_footer(); ?>