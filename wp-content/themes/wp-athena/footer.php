<?php
    $logo_img = get_field('footer_logo', 'option');
?>
<footer id="footer">
    <div class="wrapper">
        <div class="footer__container">
            <div class="footer__top">
                <div class="footer__logo">
                    <a href="<?php echo get_home_url(); ?>"><img src="<?php echo $logo_img; ?>" alt=""></a>
                </div>
                <div class="footer__menu left">
                    <?php
                    if ( has_nav_menu( 'athena_main_menu' ) ) {
                        wp_nav_menu( array(
                            'theme_location' => 'main_nav',
                            'menu' => 'footer-menu-left',
                            'menu_class' => '',
                            'container' => ''
                        ) );
                    }
                    ?>
                </div>
                <div class="footer__menu right">
                    <?php
                    if ( has_nav_menu( 'athena_main_menu' ) ) {
                        wp_nav_menu( array(
                            'theme_location' => 'main_nav',
                            'menu' => 'footer-menu-right',
                            'menu_class' => '',
                            'container' => ''
                        ) );
                    }
                    ?>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer__partner">
                    <ul>
                        <?php
                            if( have_rows('footer_image', 'option') ):
                                while( have_rows('footer_image', 'option') ): the_row();
                                    $image = get_sub_field('image');
                                    $link = get_sub_field('link');
                        ?>
                                    <li><a href="<?php echo $link; ?>"><img src="<?php echo $image['url']; ?>" alt=""></a></li>
                        <?php
                                endwhile;
                            endif;
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
