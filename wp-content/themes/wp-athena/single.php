<?php get_header(); ?>
<main id="main-content">
    <div class="wrapper">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">

            <h2><?php the_title(); ?></h2>
            <div class="content">
                <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile; endif; ?>
    </div>
    <div class="wrapper include__sugget--event">
        <div class="row">
            <div class="col-6">
                <h2 class="title__suggest">Get your event here</h2>
                <p class="description">Vestibulum mollis ex vitae erat mattis rutrum. Donec id lobortis felis. Fusce volutpat aliquet lorem, vitae vehicula sapien iaculis non. Phasellus interdum lectus ut magna tempus, ac tristique mi aliquam.</p>
            </div>
            <div class="col-6">
                <div class="botton">
                    <a href="#">Suggest an event</a>
                </div>
            </div>
        </div>
    </div>
    <p>Written by:
        <?php echo get_the_author_link(); ?></p>
    <a href="<?php echo get_author_posts_url(1); ?>"><?php the_author(); ?></a>
    <div class="wrapper">
        <?php wp_list_comments(array(
            'callback' => 'better_comments'
        )); ?>
        <?php comment_form(); ?>
    </div>
</main>
<?php get_footer(); ?>
