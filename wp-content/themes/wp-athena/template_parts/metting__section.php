<?php
    $frontpage_id = get_option( 'page_on_front' );
    $banner_week = get_field('coingeek_banner_image', $frontpage_id );
    $banner_link = get_field('coingeek_banner_link', $frontpage_id );
?>
<div class="metting__section">
    <div class="title">
        <h2 class="title__section">Where we’ll be next</h2>
        <a href="<?php echo get_post_type_archive_link('event'); ?>">All our events</a>
    </div>
    <div class="metting__section--content">
        <div class="row">
            <?php
            $args = array(
                'post_type' => 'event',
                'posts_per_page' => 4,
                'orderby' => 'date'
            );
            $query = new WP_Query($args);
            if($query->have_posts()):
                while($query->have_posts()): $query->the_post();
            ?>
                <div class="col-3">
                    <?php get_template_part('template_parts/metting__item'); ?>
                </div>

                <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
        </div>
    </div>
</div>
<div class="banner__section">
    <?php if($banner_week): ?>
        <div class="week__banner">
            <a href="<?php echo $banner_link; ?>"><img src="<?php echo $banner_week['url'] ?>" alt=""></a>
        </div>
    <?php endif; ?>
</div>