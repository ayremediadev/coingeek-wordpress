<?php
    wp_reset_query();
    $slider_items = get_field('slider_items');
?>
<div class="wrapper">
    <div class="slider">
        <?php
        foreach($slider_items as $item):
            set_query_var( 'img_new_size', IMG_SLIDER_HOME );
            $id = $item->ID;
            $post_title = get_the_title($id);
            $post_url = get_permalink($id);
            $post_description = get_the_excerpt($id);
            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($id), $img_new_size );
            $date = human_time_diff( get_the_time( 'U', $id), current_time( 'timestamp' ) ).' '.__( 'ago' );
            $terms = get_the_terms($id, 'category_new');
        ?>
            <div class="slider__wrapper">
                <div class="slider__item">
                    <div class="slider__item--content cg__radius">
                        <p class="new__info">
                            <span class="new__info--title"><?php echo $terms[0]->name; ?></span> <span class="new__info--date"><?php echo $date; ?></span>
                        </p>
                        <h2 class="title__new"><?php echo $post_title; ?></h2>
                        <div class="description">
                            <?php echo $post_description; ?>
                        </div>
                    </div>
                    <div class="slider__item--image">
                        <img src="<?php echo $image_url[0]; ?>" alt="">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>