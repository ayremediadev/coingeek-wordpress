<?php
	$post_title = get_the_title();
	$post_url = get_permalink();
	$post_description = get_the_excerpt();
	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $img_new_size );
	$date = human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
	$terms = get_the_terms($post->ID, 'category_new');
	$video = false;
	foreach($terms as $term){
		if($term->slug == 'video')
			$video = true;
	}
?>
<div class="new">
	<div class="image cg__radius">
		<img src="<?php echo $image_url[0]; ?>">
		<?php if($video): ?>
			<span class="icon-Dropdown"></span>
		<?php endif; ?>
	</div>
	<div class="new__content">
		<p class="new__info">
			<span class="new__info--title"><?php echo $terms[0]->name; ?></span> <span class="new__info--date"><?php echo $date; ?></span>
		</p>
		<p class="title__new"><?php echo $post_title; ?></p>
		<p class="description">A Pocket PC is a handheld computer, which features many of the same capabilities as a modern PC. These handy little devices allow individuals to retrieve…</p>
	</div>
	<a href="<?php echo $post_url; ?>"><?php _e('Read More', DOMAIN); ?></a>
</div>


