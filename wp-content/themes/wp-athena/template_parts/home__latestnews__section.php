<?php
    wp_reset_query();
    $sidebar_images = get_field('image_sidebar');
    $sidebar_link = empty(get_field('image_sidebar_link'))?"#":get_field('image_sidebar_link');
?>
<?php
    $number_posts = 4;
    $classes = 'col-3';
    if(is_front_page() ){
        $number_posts = 3;
    }
    $number_posts = is_front_page()?3:4;
    $classes = is_front_page()?"col-4":"col-3";
    $classes_wrapper = is_front_page()?"col-9":"col-12"
?>
<div class="news__section">
    <div class="row">
        <div class="latest__new--left <?php echo $classes_wrapper; ?>">
            <h2 class="title__section"><?php _e('lastest news', DOMAIN); ?></h2>
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'news',
                        'posts_per_page' => $number_posts,
                        'orderby' => 'date'
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                ?>
                            <div class="<?php echo $classes; ?>">
                                <?php get_template_part('template_parts/new'); ?>
                            </div>
                        <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
        <?php if(is_front_page()): ?>
        <div class="latest__new--right col-3">
            <div class="include-rigister cg__radius">
                <a href="<?php echo $sidebar_link; ?>">
                    <img src="<?php echo $sidebar_images['url']; ?>" alt="">
                </a>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>