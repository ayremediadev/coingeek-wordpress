<div class="header__menu">
    <?php
    if ( has_nav_menu( 'athena_main_menu' ) ) {
        // User has assigned menu to this location;
        // output it
        wp_nav_menu( array(
            'theme_location' => 'main_nav',
            'menu' => 'header_menu',
            'menu_class' => '',
            'container' => '',
            'link_before' => '',
            'link_after'  => '<span class="plus_icon"></span>'
        ) );
    }
    ?>
</div>