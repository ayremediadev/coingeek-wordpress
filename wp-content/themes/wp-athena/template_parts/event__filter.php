<div class="filter__event wrapper">
    <div class="wrapper">
        <div class="row">
            <div class="col-6">
                <p class="text__left">Where we’ll be next</p>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-4">
                        <h3 class="label__month">MONTH</h3>
                        <div class="month">
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                            <span class="value__month">Octobe</span>
                        </div>
                    </div>
                    <div class="col-4">
                        <h3>location</h3>
                        <div class="box__filter--location">
                            <select class="filter__location select2" data-placeholder="City or country..." name="location" >
                                <option></option>
                                <option value="AL">City</option>
                                <option value="WY">Country</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <h3>Event type</h3>
                        <div class="box__filter--type">
                            <select class="filter__type select2" data-placeholder="Meetup, talk..." name="type">
                                <option></option>
                                <option value="AL">Alabama</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>