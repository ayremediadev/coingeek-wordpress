<?php
    $post_title = get_the_title();
    $place = get_field('place');
    $date = get_field('date');
    $myDateTime = DateTime::createFromFormat('d/m/Y', $date);
    $date = $myDateTime->format('M j');
    $post_url = get_permalink();
?>
<div class="metting__item">
    <h2>
        <?php echo $post_title; ?>
    </h2>
    <p class="metting__date"><?php echo $date; ?></p>
    <p class="metting__place"><?php echo $place; ?></p>
    <a href="<?php echo $post_url; ?>"><?php _e('Read More', DOMAIN); ?></a>
</div>