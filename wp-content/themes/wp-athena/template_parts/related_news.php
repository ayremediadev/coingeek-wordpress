<?php 
    $template = 1;
    $classes = $template?"col-12":"col-3";
?>
<div class="news__section">
    <div class="row">
        <div class="latest__new--left col-12">
            <h2 class="title__section">lastest news</h2>
            <div class="row">
                <?php
                $args = array(
                    'post_type' => 'news',
                    'posts_per_page' => 4,
                    'orderby' => 'date'
                );
                $query = new WP_Query($args);
                if($query->have_posts()):
                    while($query->have_posts()): $query->the_post();
                        set_query_var( 'img_new_size', IMG_LATEST_NEWS );
                        ?>
                        <div class="<?php echo $classes; ?>">
                            <?php get_template_part('template_parts/new'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>