<?php
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), IMG_EVENT_DETAIL );
    $terms = get_the_terms($post->ID, 'category_event');
    $website_link = get_field('website_link');
?>
<?php get_header(); ?>
<main id="main-content">
    <section id="content__event--detail">
        <?php if($image_url): ?>
        <div class="banner">
            <div class="wrapper">
                <img src="<?php echo $image_url[0]; ?>" alt="">
            </div>
        </div>
        <?php endif; ?>
        <div class="main__content">
            <div class="wrapper">
                <div class="row">
                    <div class="col-8">
                        <p class="new__info">
                            <span class="new__info--title"><?php echo $terms[0]->name; ?></span>
                        </p>
                        <div class="content">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h2 class="title"><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile; endif; ?>
                        <?php if($website_link): ?>
                            <div class="botton__see--web">
                                <a href="<?php echo $website_link; ?>">See website</a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-4">
                        <img src="assets/images/register.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="wrapper">
        <?php $comments = get_comments('post_id='.get_the_ID()); var_dump($comments); ?>
        <?php comment_form(); ?>
    </div>
</main>
<?php get_template_part('template_sections/home__footer'); ?>
<?php get_footer(); ?>
